<!doctype html>
<html>
	<head>
		<title>FitNet - Create Database</title>
		  <meta charset="UTF-8">
		  <link rel="stylesheet" type="text/css" href='css/fitnetstyle.css'>
    </head>
<body >
<div class="wrapper">
	<div class="header">
	<?php
		include 'connect.php';
		$select = 'SELECT FilePath FROM BannerImages ORDER BY RAND() LIMIT 1';
		$result = mysql_query($select);
		$row = mysql_fetch_assoc($result);
		mysql_free_result($result);
		$filePath = $row['FilePath'];
		echo "<img src='$filePath' alt='an image'>";
	?>
	</div>

	<div class="content">
		<?php

		$dropQuery = "DROP DATABASE IF EXISTS mcphekt1_FitNet";
		$result = mysql_query($dropQuery);
		echo "$result<br>";//this will say 1

		$database = "mcphekt1_FitNet";

		$createDB = "CREATE DATABASE ".$database;
		$result = mysql_query($createDB) or die("Could not create database");
		echo "$result<br>";

		$db = mysql_select_db($database, $connection) or die("Couldn't select database");


		//make the users table
		$createQuery = "CREATE TABLE  Users
		(
			UserID		INT(6)	NOT NULL AUTO_INCREMENT,
			FirstName		VARCHAR(20)	NOT NULL,
			LastName		VARCHAR(20)	NOT NULL,
			Password		VARCHAR(100) NOT NULL,
			Email		VARCHAR(30)	NOT NULL,
			DisplayName		VARCHAR(15)	NOT NULL,
			LastExercised TIMESTAMP NOT NULL DEFAULT 0, 
			ProfilePicture VARCHAR(100) NOT NULL DEFAULT './Images/default.jpg',
			PRIMARY KEY(UserID)
		)engine=InnoDB";

		$result = mysql_query($createQuery);
		echo "$result";

		//make the friends table
		$createQuery = "CREATE TABLE  Friends
		(
			FriendID	INT(6)	NOT NULL,
			UserID		INT(6)	NOT NULL,
			Accepted	BOOLEAN NOT NULL DEFAULT FALSE ,
			FOREIGN KEY(FriendID) REFERENCES Users(UserID) ON UPDATE CASCADE ON DELETE CASCADE,
			FOREIGN KEY(UserID) REFERENCES Users(UserID) ON UPDATE CASCADE ON DELETE CASCADE
		)engine=InnoDB";

		$result = mysql_query($createQuery);
		echo "$result";

		//make the activity table
		$createQuery = "CREATE TABLE  Activity
		(
			ActivityID		INT(6)	NOT NULL AUTO_INCREMENT,
			ActivityName VARCHAR(20) NOT NULL,
			ColourCode VARCHAR(7) NOT NULL,
			PRIMARY KEY(ActivityID)
		)engine=InnoDB";

		$result = mysql_query($createQuery);
		echo "$result";

		//make the workout table
		$createQuery = "CREATE TABLE  Workout
		(
			WorkoutID		INT(6)	NOT NULL AUTO_INCREMENT,
			UserID INT(6) NOT NULL,
			Date DATE NOT NULL,
			TotalTime		int(10) NOT NULL,
			Distance		VARCHAR(20) DEFAULT 'N/A' NOT NULL,
			Comment		VARCHAR(256) NOT NULL,
			Area VARCHAR(20) DEFAULT 'N/A' NOT NULL,
			ActivityID INT(6) NOT NULL,
			Added TIMESTAMP DEFAULT NOW() NOT NULL,
			PRIMARY KEY(WorkoutID),
			FOREIGN KEY(UserID) REFERENCES Users(UserID) ON UPDATE CASCADE ON DELETE CASCADE,
			FOREIGN KEY(ActivityID) REFERENCES Activity(ActivityID) ON UPDATE CASCADE ON DELETE CASCADE
		) engine=InnoDB";

		$result = mysql_query($createQuery);
		echo "$result";

		//Make the goal types table
		$createQuery = "CREATE TABLE GoalTypes
		(
			ID INT(6) AUTO_INCREMENT NOT NULL,
			Type VARCHAR(20) NOT NULL,
			PRIMARY KEY(ID)
		)engine=InnoDB";

		$result = mysql_query($createQuery);
		echo "$result";

		//make the goals table
		$createQuery = "CREATE TABLE  Goals
		(
			UserID		INT(6)	NOT NULL,
			ActivityID VARCHAR(6) DEFAULT 'N/A' NOT NULL,
			GoalTypeID INT(6) NOT NULL,
			GoalName VARCHAR(50) NOT NULL,
			Value INT(6) NOT NULL,
			FOREIGN KEY(UserID) REFERENCES Users(UserID) ON UPDATE CASCADE ON DELETE CASCADE,
			FOREIGN KEY(GoalTypeID) REFERENCES GoalTypes(ID) ON UPDATE CASCADE ON DELETE CASCADE
		)engine=InnoDB";

		$result = mysql_query($createQuery);
		echo "$result";

		$createQuery = "CREATE TABLE GraphFriendSettings
		(
			UserID	INT(6) NOT NULL,
			GraphFriendID	INT(6) NOT NULL,
			FOREIGN KEY (UserID) REFERENCES Users(UserID) ON UPDATE CASCADE ON DELETE CASCADE,
			FOREIGN KEY (GraphFriendID) REFERENCES Users(UserID) ON UPDATE CASCADE ON DELETE CASCADE
		)engine=InnoDB";

		$result = mysql_query($createQuery);
		echo "$result";

		$createQuery = "CREATE TABLE WorkoutLikes
		(
			WorkoutID	INT(6) NOT NULL,
			LikerID	INT(6) NOT NULL,
			PRIMARY KEY (WorkoutID, LikerID),
			FOREIGN KEY (WorkoutID) REFERENCES Workout(WorkoutID) ON UPDATE CASCADE ON DELETE CASCADE,
			FOREIGN KEY (LikerID) REFERENCES Users(UserID) ON UPDATE CASCADE ON DELETE CASCADE
		)engine=InnoDB";

		$result = mysql_query($createQuery);
		echo "$result";

		$createQuery = "CREATE TABLE BannerImages
		(
			ID	INT(6) AUTO_INCREMENT NOT NULL,
			FilePath VARCHAR(50) NOT NULL,
			PRIMARY KEY (ID)
		)engine=InnoDB";

		$result = mysql_query($createQuery);
		echo "$result";

		CreateUser("Kierran", "McPherson", "cheeky", "kierranm@gmail.com", "KierranM");
		CreateUser("Clementine", "Trengrove", "klem", "trengcj1@student.op.ac.nz", "trengcj1");
		CreateUser("Tester", "Tester", "test", "test@test.com", "test");

		CreateActivity('Bike','#008000');
		CreateActivity('Walk','#FFFF00');
		CreateActivity('Run','#FFA500');
		CreateActivity('Surf','#3A33FF');
		CreateActivity('Swim','#FF3333');
		CreateActivity('Yoga','#FFC0CB');
		CreateActivity('Weight Training', '#8B4513');

		AddFriend("KierranM","trengcj1");
		AddFriend("KierranM", "test");
		AddFriend("trengcj1","test");

		AddWorkout("test","Bike","2013-11-05","3000",30, "N/A", "Biked for a bit");
		AddWorkout("test","Swim","2013-11-01","3000",60, "Moana Pool", "Swam for a bit");
		AddWorkout("test","Run","2013-11-07","3000",30, "The Oval", "Ran for a bit");
		AddWorkout("test","Surf","2013-10-31","N/A",60, "St. Clair", "Surfed for a bit");
		AddWorkout("test","Surf","2013-11-6","N/A",42, "St. Kilda", "Surfed for a bit less");

		AddGoalType("Total");
		AddGoalType("Duration");
		AddGoalType("Distance");
		AddGoalType("Average Duration");
		AddGoalType("Average Distance");

		AddGoal('test','N/A','Total','Do 5 exercises a week', 5);
		AddGoal('test','Surf','Total','Go surfing 4 times', 4);
		AddGoal('test','Walk','Duration','Walk for 20 minutes', 20);
		AddGoal('test','N/A','Average Duration','Do at least 1 hours of exercise per day', 60);
		AddGoal('test','Bike','Average Distance','Bike for at least 5km per day', 5000);
		AddGoal('test','Weight Training','Total','Do 5 weight sessions a week', 5);
		AddGoal('test','Swim','Distance','Swim 3km a week', 3000);

		AddBannerImage('./banners/banner1.jpg');
		AddBannerImage('./banners/banner2.jpg');
		AddBannerImage('./banners/banner3.jpg');
		AddBannerImage('./banners/banner4.jpg');
		AddBannerImage('./banners/banner5.jpg');
		AddBannerImage('./banners/banner6.jpg');
		AddBannerImage('./banners/banner7.jpg');
		AddBannerImage('./banners/banner8.jpg');
		AddBannerImage('./banners/banner9.jpg');
		AddBannerImage('./banners/banner10.jpg');
		AddBannerImage('./banners/banner11.jpg');

		function CreateUser($firstName, $lastName, $password, $email, $displayName)
		{
			$hashedPW = md5($password);
			$size = 128;
			$profilePic = "http://permissiondenied.net/identicon/hash/".$size."/".$displayName.".png";
			$insertQuery = "INSERT INTO Users (FirstName, LastName, Password, Email, DisplayName,ProfilePicture) VALUES ('$firstName', '$lastName', '$hashedPW', '$email', '$displayName','$profilePic')";

			$result = mysql_query($insertQuery);

			echo "$firstName $lastName insert result: $result <br>";
		}

		function CreateActivity($name, $color)
		{

			$insert = "INSERT INTO Activity (ActivityName, ColourCode) VALUES ('$name','$color')";

			$result = mysql_query($insert);	

			echo "$name insert result: $result<br>";
		}

		function AddWorkout($userDisplayName, $workoutName, $date, $distance, $time, $area, $comment)
		{
			$activityID = GetActivityIDByName($workoutName);
			$userID = GetUserIDFromDisplayName($userDisplayName);

			$insert = "INSERT INTO Workout(UserID,Date,TotalTime,Distance,Comment,Area,ActivityID) VALUES ($userID,'$date',$time,'$distance','$comment','$area',$activityID)";
			$update = "UPDATE Users SET LastExercised = '$date' WHERE UserID = $userID";
			$result = mysql_query($insert);

			echo "Adding workout for $userDisplayName result: $result<br>";
			$result = mysql_query($update);
		}

		function AddGoalType($type)
		{
			$insert = "INSERT INTO GoalTypes (Type) VALUES ('$type')";

			$result = mysql_query($insert);

			echo "Adding goal type $type result: $result<br>";
		}

		function AddGoal($userDisplayName, $activityName, $goalType, $goalName, $goalValue)
		{
			$uid = GetUserIDFromDisplayName($userDisplayName);
			if ($activityName != 'N/A') {
				$activityID = GetActivityIDByName($activityName);
			}
			else{
				$activityID = 'N/A';
			}
			$gtID = GetGoalTypeFromName($goalType);


			$insert = "INSERT INTO Goals (UserID, ActivityID, GoalTypeID, GoalName, Value) VALUES ($uid,'$activityID','$gtID','$goalName',$goalValue)";

			$result = mysql_query($insert);

			echo "Adding goal [$goalName] for $userDisplayName result: $result<br>";
		}

		function GetActivityIDByName($activityName)
		{
			$select = "SELECT ActivityID FROM Activity WHERE ActivityName = '$activityName'";
			$result = mysql_query($select);
			$row = mysql_fetch_row($result);
			$aid = $row[0];

			return $aid;
		}

		//Given two display names it adds them as friends
		function AddFriend($userDN,$friendDN)
		{
			$userID = GetUserIDFromDisplayName($userDN);
			$friendID = GetUserIDFromDisplayName($friendDN);

			$insert = "INSERT INTO Friends (UserID, FriendID, Accepted) VALUES ($userID,$friendID,FALSE)";
			$result = mysql_query($insert);

			echo "Adding friends $userDN and $friendDN result: $result<br>";
		}

		function AddBannerImage($filepath)
		{
			$insert = "INSERT INTO BannerImages (FilePath) VALUES ('$filepath')";
			$result = mysql_query($insert);

			echo "Adding banner image $filepath: result";
		}

		function GetUserIDFromDisplayName($displayName)
		{
			$select = "SELECT UserID FROM Users WHERE DisplayName = '$displayName'";
			$result = mysql_query($select);

			$row = mysql_fetch_row($result);
			$uid = $row[0];

			return $uid;
		}

		function GetGoalTypeFromName($name)
		{
			$select = "SELECT ID FROM GoalTypes WHERE Type = '$name'";
			$result = mysql_query($select);

			$row = mysql_fetch_row($result);
			$gtid = $row[0];

			return $gtid;
		}

		?>
		</div>

		<div class="footer">
			
		</div>
	</div>
</body>
</html>
 