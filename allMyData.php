<?php
	include "accessControl.php";
?>
<!--
	Purpose: This page displays the summarized data for the logged in user
-->
<!DOCTYPE html>
<html>
<head>
	<title>FitNet - All My Data</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/fitnetstyle.css">
</head>
	<body>
		<div class="wrapper">
			<div class="header">
				<?php
					//Choose a random banner to display
					include 'connect.php';
					$select = 'SELECT FilePath FROM BannerImages ORDER BY RAND() LIMIT 1';
					$result = mysql_query($select);
					$row = mysql_fetch_assoc($result);
					mysql_free_result($result);
					$filePath = $row['FilePath'];
					echo "<img src='$filePath' alt='an image'>";
				?>
			</div>

			<div class="content">
				<?php
					include 'navbar.php';
					include 'profileNav.php';
					$myID = $_SESSION['userID'];

					ShowLifeTimeStats($myID);
					ShowSummaryTable($myID);

					//Shows the lifetime stats of the user with the given ID
					function ShowLifeTimeStats($myID)
					{
						echo "<h2 class='noPadding noMargin'>My LifeTime Stats</h2>";
						echo "<hr>";

						//Select the values for the Users workouts
						$select = "SELECT * FROM Workout WHERE UserID = $myID";

						$result = mysql_query($select);

						$totalWorkouts = mysql_num_rows($result);
						$totalTime = 0;
						$totalDistance = 0;
						while ($row = mysql_fetch_assoc($result)) {
							$totalTime += $row['TotalTime'];
							if ($row['Distance'] != "N/A") {
								$totalDistance += $row['Distance'];
							}
						}

						$totalDistanceInKM = round($totalDistance/1000,2);
						$totalTimeInHours = round($totalTime/60,2);

						ShowBubble("Total Workouts", 'total.png', $totalWorkouts);
						ShowBubble("Total Time", 'time.png', $totalTimeInHours, " hours");
						ShowBubble("Total Distance", 'distance.png', $totalDistanceInKM, "km");

						mysql_free_result($result);
					}//End ShowLifeTimeStats()
					
					//Shows a bubble containing the given information
					function ShowBubble($name, $image, $value, $suffix = '')
					{
						echo "<div class='box lifeStat'>";
							echo "<div>";
								echo "<img style='float: left;margin-right:10px;' src='./Images/$image' height='64' width='64'>";
							echo "</div>";
							echo "<h3 class='noPadding noMargin' style='margin-top:10px;'>$name</h3>";
							echo "<br>";
							echo "<b>$value$suffix</b>";
						echo "</div>";
					}//End ShowBubble()

					//Shows a table that summarizes all the data for each activity type
					function ShowSummaryTable($myID)
					{
						echo "<h2 class='noPadding noMargin'>My Summary</h2>";
						echo "<hr>";
						$selectActivities = "SELECT ActivityID, ActivityName FROM Activity";
						$activityResult = mysql_query($selectActivities);

						echo "<table class='bordered centered dataTable'>";
							echo "<tr>";
								echo "<th class='bordered centered'>Activity Name<br></th>";
								echo "<th class='bordered centered'>No. Workouts<br></th>";
								echo "<th class='bordered centered'>Total Time<br><i class='smallText noPadding noMargin'>Hours</i></th>";
								echo "<th class='bordered centered'>Avg. Minutes<br></th>";
								echo "<th class='bordered centered'>Total Distance<br><i class='smallText noPadding noMargin'>Kilometres</i></th>";
								echo "<th class='bordered centered'>Avg. Distance<br><i class='smallText noPadding noMargin'>Of distances entered</i></th>";
							echo "</tr>";

						//Loop for each activity type
						while ($activity = mysql_fetch_assoc($activityResult)) {
							$activityID = $activity['ActivityID'];

							//Select all the workouts of this activity type for this user
							$selectWorkouts = "SELECT * FROM Activity AS a JOIN Workout AS w ON w.ActivityID = a.ActivityID WHERE w.UserID = $myID AND w.ActivityID = $activityID";

							$workoutResult = mysql_query($selectWorkouts);

							$acTotalWorkouts = mysql_num_rows($workoutResult);
							$acName = $activity['ActivityName'];
							$acTotalMinutes = 0;
							$acAverageMinutes = 0;
							$acTotalDistance = 0;
							$acAverageDistance = 0;
							$acADCount = 0;
							$acTotalKM = 0;
							$acTotalHours = 0;


							if ($acTotalWorkouts > 0) {
								while ($workout = mysql_fetch_assoc($workoutResult)) {
									$acTotalMinutes += $workout['TotalTime'];
									$acAverageMinutes += $workout['TotalTime'];
									$acTotalDistance += $workout['Distance'];
									if ($workout['Distance'] != "N/A") {
										$acAverageDistance += $workout['Distance'];
										$acADCount++;
									}
								}

								$acTotalHours = round($acTotalMinutes/60,2);
								$acTotalKM = round($acTotalDistance/1000,2);
								$acAverageMinutes = $acAverageMinutes/$acTotalWorkouts;
								if ($acADCount > 0) {
									$acAverageDistance = round($acAverageDistance/$acADCount,2);
								}
								
							}

							echo "<tr>";
								echo "<td class='bordered centered'>$acName</td>";
								echo "<td class='bordered centered'>$acTotalWorkouts</td>";
								echo "<td class='bordered centered'>$acTotalHours</td>";
								echo "<td class='bordered centered'>$acAverageMinutes</td>";
								echo "<td class='bordered centered'>$acTotalKM</td>";
								echo "<td class='bordered centered'>$acAverageDistance</td>";
							echo "</tr>";
						}
						echo "</table>";
						mysql_free_result($activityResult);
						mysql_free_result($workoutResult);
					}//End ShowSummaryTable()
				?>
				
			</div>

			<div class="footer">
			</div>
		</div>
	</body>
</html>