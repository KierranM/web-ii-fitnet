<?php
	include "accessControl.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>FitNet - Goals</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href='css/fitnetstyle.css'>
	<script src='javascript/jquery.js' type="text/javascript" charset="utf-8"></script>
	<script src='javascript/evenOutProfilePic.js' type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src='javascript/autoChangeGoalValue.js'></script>
</head>
	<body>
		<div class="wrapper">
			<div class="header">
			<?php
				include 'connect.php';
				$select = 'SELECT FilePath FROM BannerImages ORDER BY RAND() LIMIT 1';
				$result = mysql_query($select);
				$row = mysql_fetch_assoc($result);
				mysql_free_result($result);
				$filePath = $row['FilePath'];
				echo "<img src='$filePath' alt='an image'>";
			?>
			</div>

			<div class="content">
				<?php
					include "navbar.php";
					$myID = $_SESSION['userID'];

					if (isset($_POST['submit'])) {
						$activityID = strip_tags($_POST['activityID']);
						$goalType = strip_tags($_POST['goalType']);
						$goalName = strip_tags($_POST['goalName']);
						$goalValue = strip_tags($_POST['goalValue']);

						if ($goalName == "") {
							$goalTypeName = GetGoalTypeFromID($goalType);
							$name = "";
							if ($activityID == "N/A") {
								$name = "All";
							}
							else{
								$name = GetActivityNameByID($activityID);
							}

							$goalName = "Achieve a(n) $goalTypeName of $goalValue for $name";
						}
						

						$insert = "INSERT INTO Goals (UserID,ActivityID,GoalTypeID,GoalName,Value) VALUES ($myID,'$activityID',$goalType,'$goalName',$goalValue)";
						$result = mysql_query($insert);
					}

					echo "<br><br>";
					ShowNewGoalForm();
					echo "<br>";
					ShowGoals($myID);

					function ShowNewGoalForm()
					{
						echo "<h2 class='noPadding noMargin'>Add A Goal</h2>";
						echo "<hr>";
						echo "<form action='goals.php' method='POST'>";
							echo "<label for='activity'>Activity: </label>";
							echo "<select id='activity' name='activityID'>";
								echo "<option value='N/A'>All</option>";
								$selectAllActivities = "SELECT * FROM Activity";
								$result = mysql_query($selectAllActivities);
								while ($row = mysql_fetch_assoc($result)) {
									echo "<option value='".$row['ActivityID']."'>".$row['ActivityName']."</option>";
								}
								mysql_free_result($result);
							echo "</select>";

							echo "<label for='goaltype'>  Goal Type: </label>";
							echo "<select id='goaltype' name='goalType'>";
								$selectAllGoalTypes = "SELECT * FROM GoalTypes";
								$result = mysql_query($selectAllGoalTypes);
								while ($row = mysql_fetch_assoc($result)) {
									echo "<option value='".$row['ID']."'>".$row['Type']."</option>";
								}
								mysql_free_result($result);
							echo "</select>";

							echo "<label for='goalname'>  Goal Name: </label>";
							echo "<input type='text' id='goalname' name='goalName' maxlength='50' placeholder='Run 10 times'>";

							echo "<label for='goalvalue'>  Value: </label>";
							echo "<input type='number' id='goalvalue' name='goalValue' min=1 value=1 required>";
							echo "<label id='valueTypeDisplay' for='goalvalue'>workouts</label><br>";
							echo "<input type='submit' name='submit' value='Add Goal'>";
						echo "</form>";
						echo "<hr>";
					}

					function ShowGoals($myID)
					{
						echo "<h2 class='noPadding noMargin'>My Goals</h2>";
						echo "<hr>";
						$selectMyGoals = "SELECT * FROM Goals WHERE UserID = $myID";
						$result = mysql_query($selectMyGoals);
						if (mysql_num_rows($result) > 0) {
							while ($row = mysql_fetch_assoc($result)) {
								ShowGoal($myID, $row);
							}
						}
						else{
							echo "<b>You have no goals, try setting some above</b>";
						}
					}

					function ShowGoal($myID, $values)
					{
						$activityID = $values['ActivityID'];
						$goalTypeID = $values['GoalTypeID'];
						$goalName = $values['GoalName'];
						$goalValue = $values['Value'];
						$monday = strtotime('last monday', strtotime('tomorrow'));
						$date = date("Y-m-d", $monday);

						$select = "";
						if ($activityID == "N/A") {
							$select = "SELECT * FROM Workout WHERE UserID = $myID";
						}
						else
						{
							$select = "SELECT * FROM Workout WHERE UserID = $myID AND ActivityID = $activityID AND Date >= '$date'";
						}

						$result = mysql_query($select);

						$goalType = GetGoalTypeFromID($goalTypeID);

						$value = 0;
						switch ($goalType) {
							case 'Total':
								$value = mysql_num_rows($result);
								break;
							case 'Duration':
								while ($row = mysql_fetch_assoc($result)) {
									$value += $row['TotalTime'];
								}
								break;
							case 'Distance':
								while ($row = mysql_fetch_assoc($result)) {
									if ($row['Distance'] != 'N/A') {
										$value += $row['Distance'];
									}
								}
								break;
							case 'Average Duration':
								$count = 0;
								$tempVal = 0;
								while ($row = mysql_fetch_assoc($result)) {
									$tempVal += $row['TotalTime'];
									$count++;
								}
								if ($count == 0) {
									$value = $tempVal/$count;
								}
								break;
							case 'Average Distance':
								$count = 0;
								$tempVal = 0;
								while ($row = mysql_fetch_assoc($result)) {
									if ($row['Distance'] != 'N/A') {
										$tempVal += $row['Distance'];
										$count++;
									}
								}
								if ($count == 0) {
									$value = $tempVal/$count;
								}
								
								break;
							default:
								$value = 0;
								break;
						}
						echo "<div class='goal box'>";
							echo "<b>$goalName</b><br>";
							echo "<b>Progress: </b>";
							if ($value >= $goalValue) {
								echo "<br>";
								echo "<br>";
								echo "<h4 class='noPadding noMargin'>Completed</h4>";
							}
							else{
								echo "<b>$value/$goalValue</b><br>";
								echo "<progress max='$goalValue' value='$value'>";
							}
						echo "</div>";
					}

					function GetGoalTypeFromID($gtid)
					{
						$select = "SELECT Type FROM GoalTypes WHERE ID = $gtid";
						$result = mysql_query($select);

						$row = mysql_fetch_assoc($result);
						$goalType = $row['Type'];

						mysql_free_result($result);
						return $goalType;
					}

					function GetActivityNameByID($ActivityID)
					{
						$select = "SELECT ActivityName FROM Activity WHERE ActivityID = $ActivityID";
						$result = mysql_query($select);

						$row = mysql_fetch_assoc($result);
						$activityName = $row['ActivityName'];

						mysql_free_result($result);
						return $activityName;
					}
				?>
			</div>

			<div class="footer">
			</div>
		</div>
	</body>
</html>