<?php 

$currentPage = basename($_SERVER['PHP_SELF']);
echo "<nav class='main'>";
	if($currentPage != "home.php"){
		echo "<a class='links navBoxLeftEdge' href='home.php'>Home</a>";
	}
	else{
		echo "<b class='links current navBoxLeftEdge'>Home</b>";
	}
	if($currentPage != "myProfile.php" && $currentPage != "allMyData.php" && $currentPage != "myRawData.php"){
		echo "<a class='links navBoxCenter' href='myProfile.php'>My Profile</a>";
	}
	else{
		echo"<b class='links current navBoxCenter'>My Profile</b>";
	}
	if($currentPage != "addActivity.php"){
		echo "<a class='links navBoxCenter' href='addActivity.php'>Add Workout</a>";
	}
	else{
		echo "<b class='links current navBoxCenter'>Add Workout</b>";
	}
	if($currentPage != "myCalendar.php"){
		echo "<a class='links navBoxCenter' href='myCalendar.php'>My Calendar</a>";
	}
	else{
		echo "<b class='links current navBoxCenter'>My Calendar</b>";
	}
	if($currentPage != "friends.php"){
		echo "<a class='links navBoxCenter' href='friends.php'>My Friends</a>";
	}
	else{
		echo "<b class='links current navBoxCenter'>My Friends</b>";
	}
	if($currentPage != "goals.php"){
		echo "<a class='links navBoxCenter' href='goals.php'>Goals</a>";
	}
	else{
		echo "<b class='links current navBoxCenter'>Goals</b>";
	}
	if($currentPage != "logout.php"){
		echo "<a class='links navBoxRightEdge' href='logout.php'>Logout</a>";
	}
	else{
		echo "<b class='links current navBoxRightEdge'>Logout</b>";
	}
	
echo "</nav>";
?>