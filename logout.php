<?php
	session_start();
	session_destroy();
?>
<!DOCTYPE html>
<html>
<head>
	<title>FitNet - Goodbye</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href='css/fitnetstyle.css'>
	<script src='javascript/jquery.js' type="text/javascript" charset="utf-8"></script>
	<script src='javascript/logout.js' type="text/javascript" charset"utf-8"></script>
</head>
	<body>
		<div class="wrapper">
			<div class="header">
				<?php
					include 'connect.php';
					$select = 'SELECT FilePath FROM BannerImages ORDER BY RAND() LIMIT 1';
					$result = mysql_query($select);
					$row = mysql_fetch_assoc($result);
					mysql_free_result($result);
					$filePath = $row['FilePath'];
					echo "<img src='$filePath' alt='an image'>";
				?>
			</div>

			<div class="content">
				<h1>You have been logged out</h1>
				<b>Sending you to the login page in <b id='timeleft'>3</b> seconds</b>
			</div>

			<div class="footer">
			</div>
		</div>
	</body>
</html>