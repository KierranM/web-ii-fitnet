<?php
	include "accessControl.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>FitNet - Show Tables</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href='css/fitnetstyle.css'>
</head>
	<body>
		<div class="wrapper">
			<div class="header">
			<img src="./Images/fitnetbanner.jpg" alt="an image">
			</div>

			<div class="content">
				<?php 
					include 'connect.php';

					ShowTable("Users");
					ShowTable("Friends");
					ShowTable("Workout");
					ShowTable("GoalTypes");
					ShowTable("Goals");
					ShowTable("Activity");
					ShowTable('WorkoutLikes');

					function ShowTable($tableName)
					{
						$query = "SELECT * FROM $tableName";
						$result = mysql_query($query);
						if ($result != 0) {
							echo "<h3>Showing $tableName table</h3>";
							$values = mysql_fetch_assoc($result);

							echo "<table class='bordered'>";
								echo "<tr>";
									foreach (array_keys($values) as $key) {
										echo "<th class='bordered'>$key</th>";
									}
								echo "</tr>";
								echo "<tr>";
									foreach ($values as $key => $value) {
										if ($key == "ColourCode") {
											echo "<td style='background-color:$value;' class='bordered'>$value</td>";	
										}
										else{
											if ($key == "ProfilePicture") {
												echo "<td class='bordered'><img src='$value' height='32' width='32'></td>";
											}
											else{
												if ($key == "Password") {
													echo "<td class='bordered'>Super secret password</td>";
												}
												else{
													echo "<td class='bordered'>$value</td>";	
												}	
											}	
										}	
									}
								echo "</tr>";
								while ($row = mysql_fetch_assoc($result)) {
									echo "<tr>";
										foreach ($row as $key => $value) {
											if ($key == "ColourCode") {
												echo "<td style='background-color:$value;' class='bordered'>$value</td>";	
											}
											else{
												if ($key == "ProfilePicture") {
													echo "<td class='bordered'><img src='$value' height='32' width='32'></td>";
												}
												else{
													if ($key == "Password") {
														echo "<td class='bordered'>Super secret password</td>";
													}
													else{
														echo "<td class='bordered'>$value</td>";	
													}			
												}
											}

										}
									echo "</tr>";
								}
							echo "</table>";
						}
						else{
							echo "<h3>Table $tableName could not be displayed</h3>";
						}
						
					}
				 ?>
			</div>

			<div class="footer">
			</div>
		</div>
	</body>
</html>