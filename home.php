<?php
	include "accessControl.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>FitNet - Home</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href='css/fitnetstyle.css'>
	<script src='javascript/jquery.js' type="text/javascript" charset="utf-8"></script>
	<script src='javascript/evenOutProfilePic.js' type="text/javascript" charset="utf-8"></script>
</head>
	<body>
		<div class="wrapper">
			<div class="header">
			<?php
				include 'connect.php';
				$select = 'SELECT FilePath FROM BannerImages ORDER BY RAND() LIMIT 1';
				$result = mysql_query($select);
				$row = mysql_fetch_assoc($result);
				mysql_free_result($result);
				$filePath = $row['FilePath'];
				echo "<img src='$filePath' alt='an image'>";
			?>
			</div>

			<div class="content">
				<?php
					error_reporting(E_ERROR);
					include "navbar.php";
					define('INCREASE', 10);
					
					$startNum = 0;

					if (isset($_POST['like'])) {
						$IDofWorkoutToLike = strip_tags($_POST['workoutID']);
						$myID = $_SESSION['userID'];
						$updateValue = "INSERT INTO WorkoutLikes (WorkoutID, LikerID) VALUES ($IDofWorkoutToLike,$myID)";
						$result = mysql_query($updateValue);
					}
					elseif (isset($_POST['unlike'])) {
						$IDofWorkoutToUnlike = strip_tags($_POST['workoutID']);
						$myID = $_SESSION['userID'];
						$delete = "DELETE FROM WorkoutLikes WHERE WorkoutID = $IDofWorkoutToUnlike AND LikerID = $myID";
						$result = mysql_query($delete);
					}
					elseif (isset($_POST['newer'])) {
						$startNum = strip_tags($_POST['startNum']) - INCREASE;
					}
					elseif (isset($_POST['older'])) {
						$startNum = strip_tags($_POST['startNum']) + INCREASE;
					}

					GetWeeklyWorkoutTotal($_SESSION['userID']);
					echo "<table class = 'titlesHomePage'>";
					echo "<tr>";
					echo "<td>";
					echo "News Feed";
					echo "</td>";	
					echo "<td class = 'scoreCell'>";
					echo "Score Board";
					echo "</td>";
					echo "</tr>";
					echo "</table>";
					echo "<hr>";
					echo "<br>";
					GetLeaders($_SESSION['userID']);
					GetMineAndFriendsWorkouts($_SESSION['userID'], $startNum);
					
					
					function GetWeeklyWorkoutTotal($uid)
					{
					$monday = strtotime('last monday', strtotime('tomorrow'));
					$date = date("Y-m-d", $monday);
					$select = "SELECT COUNT(*) FROM Workout WHERE UserID = $uid AND Date >= $date GROUP BY UserID";
					$result = mysql_query($select);
					$row = mysql_fetch_row($result);
					$value = $row[0];
					$select = "SELECT FirstName FROM Users WHERE UserID = $uid";
					$result = mysql_query($select);
					$row = mysql_fetch_row($result);
					$valueName = $row[0];
					if (isset($value) && $value > 0) {
						echo "<h2>Welcome $valueName, you have done a total of $value workouts this week. </h2>";
					}
					else
					{
						echo "<h2>Welcome $valueName, you haven't done any workouts this week.</h2>";
					}
					
					}
					
					

					function GetMineAndFriendsWorkouts($uid, $startNum)
					{
						$rowsToGet = INCREASE;
						$selectWorkouts = 
						"(	
							SELECT u.UserID, u.DisplayName, u.ProfilePicture, w.WorkoutID, w.Comment, w.TotalTime, w.Distance, w.Date, w.Added, w.Area, a.ActivityName 
							FROM Activity as a JOIN Workout as w
							ON a.ActivityID = w.ActivityID
							JOIN Users as u 
							ON u.UserID = w.UserID 
							JOIN Friends as f 
							ON u.UserID = f.FriendID 
							JOIN Users as me 
							ON me.UserID = f.UserID 
							WHERE me.UserID = $uid
							AND f.Accepted = TRUE
						) 
						UNION 
						(	
							SELECT u.UserID, u.DisplayName, u.ProfilePicture, w.WorkoutID, w.Comment, w.TotalTime, w.Distance,w.Date, w.Added, w.Area, a.ActivityName 
							FROM Activity as a JOIN Workout as w
							ON a.ActivityID = w.ActivityID
							JOIN Users as u 
							ON u.UserID = w.UserID 
							WHERE u.UserID = $uid
						) 
						ORDER BY Date DESC, Added DESC
						LIMIT $startNum, $rowsToGet";

						$result = mysql_query($selectWorkouts);
						$numRows = mysql_num_rows($result);
						
						if ($result > 0) {
							echo "<div class='workoutsContainer'>";
								while ($row = mysql_fetch_assoc($result)) {
									$workoutID = $row['WorkoutID'];
									$selectLikes = "SELECT COUNT(*) AS LikeCount FROM WorkoutLikes WHERE WorkoutID = $workoutID";
									$rs = mysql_query($selectLikes);
									$rw = mysql_fetch_assoc($rs);
									$likeCount = $rw['LikeCount'];
									ShowWorkout($row, $likeCount, $uid);
								}
								mysql_free_result($result);
							echo "</div>";
							echo "<form class='activityFeedForm' action='home.php' method='POST'>";
								echo "<input type='hidden' name='startNum' value='$startNum'>";
								if ($startNum > 0) {
									echo "<input class='linkButton' type='submit' name='newer' value='Newer'>";
								}
								if ($numRows == INCREASE) {
									echo "<input class='linkButton' type='submit' name='older' value='Older'>";
								}
							echo "</form>";
						}
						else{
							echo "<h2>No workouts! Get out there and exercise!</h2>";
						}
						
						
					}

					function ShowWorkout($values, $likeCount, $myID)
					{
						$displayName = $values['DisplayName'];
						$profilePic = $values['ProfilePicture'];
						$comment = $values['Comment'];
						$likes = $likeCount;
						$date = $values['Date'];
						$workoutID = $values['WorkoutID'];
						$activityName = $values['ActivityName'];
						$timeTaken = round($values['TotalTime']/60,2);
						$distance = round($values['Distance']/1000,2);
						$userID = $values['UserID'];
						$area = $values['Area'];

						$d = strtotime($date);
						$betterDate = date("F j, Y",  $d);

						echo "<form action='home.php' method='POST'>";
							echo "<div class='homeWorkout box'>";
								echo "<div class='profilePic'>";
									echo "<img src='$profilePic' height='32' width='32'>";
								echo "</div>";
								if ($userID != $myID) {
									echo "<a class='friendLink'  href='friendProfile.php?id=$userID'><b>$displayName</b></a><br>";
								}
								else{
									echo "<b>$displayName</b><br>";
								}
								echo "<p class='workoutComment'>$comment <i class='smallText'> + Activity: $activityName, Time: $timeTaken hrs";
								if ($area != "N/A") {
									echo ", Location: $area";
								}
								if ($distance != "N/A") {
									echo ", Distance: $distance km";
								}
								echo "</i></p>";
								echo "<i class='datesNewsFeed'>On $betterDate</i>";
								echo "<i class='workoutInfo datesNewsFeed'>Likes: $likes</i>";
								echo "<input type='hidden' name='workoutID' value='$workoutID'>";
								if (HasUserLikedWorkout($workoutID, $_SESSION['userID'])) {
									echo "<input type='submit' name='unlike' value='Unlike' class='datesNewsFeed linkButton'>";
								}
								else{
									echo "<input type='submit' name='like' value='Like' class='datesNewsFeed linkButton'>";
								}
							echo "</div>";
						echo "</form>";

					}

					function HasUserLikedWorkout($workoutID, $userID)
					{
						$select = "SELECT * FROM WorkoutLikes WHERE WorkoutID = $workoutID AND LikerID = $userID";
						$result = mysql_query($select);
						$numRows = mysql_num_rows($result);
						if ($numRows > 0) {
							return true;
						}
						else{
							return false;
						}
					}
					
					function GetLeaders($uid)
					{
					$selectLeaders = 
						"(	
							SELECT u.UserID, u.DisplayName, SUM(w.TotalTime) AS TotalTime 
							FROM Workout as w JOIN Users as u 
							ON u.UserID = w.UserID 
							JOIN Friends as f 
							ON u.UserID = f.FriendID 
							JOIN Users as me 
							ON me.UserID = f.UserID 
							WHERE me.UserID = $uid
							AND f.Accepted = TRUE
							GROUP BY u.DisplayName
						) 
						UNION 
						(	
							SELECT u.UserID, u.DisplayName, SUM(w.TotalTime) AS TotalTime 
							FROM Workout as w JOIN Users as u 
							ON u.UserID = w.UserID 
							WHERE u.UserID = $uid
							GROUP BY u.DisplayName
						) 
						
						ORDER BY TotalTime DESC
						LIMIT 15";

						$result = mysql_query($selectLeaders);
						echo"<table class = 'homeLeaders leadersBox'>";
						$count = 1;
						echo"<tr>";
							echo "<th class='leadersBordered'>Ranking</th>";
							echo "<th class='leadersBordered'>Person</th>";
							echo "<th class='leadersBordered'>Total Time</th>";
						echo "</tr>";
						
						while ($row = mysql_fetch_assoc($result))
						{
						echo"<tr>";
						ShowLeaders($row, $count, $uid);
						
						echo "</tr>";
						$count++;
						}
						echo"</table>";

						
						mysql_free_result($result);
					}
					
					function ShowLeaders($values, $ranking, $myID)
					{
						$displayName = $values['DisplayName'];
						$totalTime = $values['TotalTime'];
						$userID = $values['UserID'];
						

						echo "<td class = 'leadersBordered rankingText'>$ranking</td>";
						if ($userID != $myID) {
							echo "<td class = 'leadersBordered'><a class='friendLink' href='friendProfile.php?id=$userID'>$displayName</a></td>";
						}
						else{
							echo "<td class = 'leadersBordered'>$displayName</td>";
						}
						echo "<td class='workoutComment leadersBordered'>$totalTime</td>";
						

					}
				?>
			</div>

			<div class="footer">
			</div>
		</div>
	</body>
</html>