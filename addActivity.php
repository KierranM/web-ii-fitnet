<?php
	include "accessControl.php";
?>
<!----
	Purpose: Allows users to add workouts to the database if they are logged in.
-->
<!DOCTYPE html>
<html>
<head>
	<title>FitNet - Add Workout</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href='css/fitnetstyle.css'>
	<script type="text/javascript" src='javascript/jscolor/jscolor.js'></script>
	<script src='javascript/jquery.js' type="text/javascript" charset="utf-8"></script>
	<script src='javascript/newActivity.js' type="text/javascript" charset="utf-8"></script>
	<script src='javascript/alignFormInputs.js' type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  	<script src='http://code.jquery.com/jquery-1.9.1.js'></script>
  	<script src='http://code.jquery.com/ui/1.10.3/jquery-ui.js'></script>
  	<script>
  	$(function() {
  	  $( "#datepicker" ).datepicker({
    	  showOn: "both",
    	  buttonImage: "Images/calendar.gif",
    	  buttonImageOnly: true,
    	  minDate: "-1w",
    	  maxDate: "d"
    	}).datepicker( "option", "dateFormat", "dd-mm-yy" ).datepicker('setDate', 'd');
  	});
  </script>

</head>
	<body>
		<div class="wrapper">
			<div class="header">
				<?php
					//choose a random banner
					include 'connect.php';
					$select = 'SELECT FilePath FROM BannerImages ORDER BY RAND() LIMIT 1';
					$result = mysql_query($select);
					$row = mysql_fetch_assoc($result);
					mysql_free_result($result);
					$filePath = $row['FilePath'];
					echo "<img src='$filePath' alt='an image'>";
				?>
			</div>

			<div class="content">
				<?php
					include 'navbar.php';

					echo "<br>";

					//If the submit button was pushed
					if (isset($_POST['submit'])) {
						//Get the data from POST
						$uid = $_SESSION['userID'];
						$activity = strip_tags($_POST['activity']);
						$date = strip_tags($_POST['date']);

						//Convert the date into a valid MySQL format
						$temp = strtotime($date);
						$temp = getdate($temp);
						$date = $temp['year'] . "-" . $temp['mon'] . "-" . $temp['mday'];

						$timeExercised = strip_tags($_POST['timeExercised']);
						$distanceTravelled = strip_tags($_POST['distanceTravelled']);
						if ($distanceTravelled == 0) {
							$distanceTravelled = "N/A";
						}
						$location = strip_tags($_POST['location']);
						$comment = strip_tags($_POST['comment']);


						//If the user is adding a new activity
						if ($activity == "newActivity") {
							//Get the new activity values from POST
							$newActivityName = strip_tags($_POST['activityName']);
							$newActivityName = addslashes($newActivityName);
							$newActivityColor = strip_tags($_POST['activityColor']);

							//Check if they are adding a duplicate activity
							$checkForDupe = "SELECT * FROM Activity WHERE BINARY ActivityName = '$newActivityName'";
							$result = mysql_query($checkForDupe);

							//If there are no activities like this then insert it into the database
							if ($result != 0) {
								$insert = "INSERT INTO Activity (ActivityName, ColourCode) VALUES ('$newActivityName', '$newActivityColor')";

								$result = mysql_query($insert);

								//If the insert failed
								if ($result == 0) {
									echo "<h2>There was a server error sorry :(</h2>";
									exit();
								}
								else//The insert succeded
								{
									//Get the ID of the new activity
									$activityID = mysql_insert_id();
									$activity = $newActivityName;
								}
							}
							else//
							{
								//There was a duplicate Activity
								$row = mysql_fetch_assoc($result);
								$activityID = $row['ActivityID'];
								$activityName = $row['ActivityName'];
							}
						} 
						else //The user chose an existing activity
						{
							//Select the activityID of the chosen activity
							$select = "SELECT activityID FROM Activity WHERE ActivityName = '$activity'";

							$result = mysql_query($select);

							//If the activity could not be found
							if ($result == 0) {
								echo "<h2>There was a server error sorry :(</h2>";
								exit();
							}
							else//The activity was found
							{
								$row = mysql_fetch_row($result);
								//Store the activityID
								$activityID = $row[0];
							}
						}

						//If the comment was empty
						if ($comment == "") 
						{
							//Retrieve the users information from the database
							$query = "SELECT * FROM Users WHERE UserID = '$uid'";

							$result = mysql_query($query);
							$user = mysql_fetch_assoc($result);
							//Build a comment using the given information
							$comment = $user['FirstName'] . " went for a " . $activity . " for " . $timeExercised . " minutes ";

							//if the location was set
							if ($location != "N/A") 
							{
								//Add it to the comment
								$comment .= "at " . $location . " ";
							}

							//If the distance was set
							if ($distanceTravelled != "N/A") 
							{
								//Add it to the comment
								$comment .= "and travelled " . $distanceTravelled . " metres ";
							}
						}
						else//the user specified a comment
						{
							$comment = addslashes($comment);
						}

						//Insert the workout into the database
						$insertWorkout = "INSERT INTO Workout (UserID, Date, TotalTime, Distance, Comment, Area, ActivityID) VALUES ('$uid','$date', '$timeExercised', '$distanceTravelled', '$comment', '$location', '$activityID')";

						$result = mysql_query($insertWorkout);

						//Check if the insert succedded or not
						if ($result == 0) {
							echo "<h2>There was an issue adding your workout, please try again later</h2>";
						}
						else//The insert succedded
						{
							//Update the LastExercised value of the user
							$myID = $_SESSION['userID'];
							$update = "UPDATE Users SET LastExercised = '$date' WHERE UserID = $myID";
							$result = mysql_query($update);
							echo "<h3>Your workout was succesfully added</h3>";
						}
					}//End submit

					ShowForm();
					
					//Display the form to add exercises
					function ShowForm()
					{
						echo "<h2 class='noPadding noMargin'>Add Workout</h2>";
						echo "<hr>";
						echo "<form action='addActivity.php' method='POST'>";
							echo "<label for='activity'>Choose Activity: </label>";
							echo "<select id='activity' name='activity'>";
								//Prefil the list of activities
								$activities = "SELECT ActivityName FROM Activity";
								$result = mysql_query($activities);
								while ($row = mysql_fetch_row($result)) {
									echo "<option value='$row[0]'>$row[0]</option>";
								}
								echo "<option value='newActivity'>New Activity</option>";
							echo "</select><br>";
							echo "<div class='newActivity'>";
								echo "<label for='activityName'>New Activity Name: </label>";
								echo "<input type='text' id='activityName' name='activityName' maxlength='20'><b id='warning' class='incorrect hidden'>Must enter a value</b><br>";
								echo "<label for='activityColor'>New Activity Chart Colour: </label>";
								echo "<input type='text' class='color {hash:true, slider:false,minS:0.6}' id='activityColor' name='activityColor' value='#66FF00'><br>";
							echo "</div>";
							echo "<label for='datepicker'>Enter the date: </label>";
							echo "<input type='text' id='datepicker' name='date'><br>";
							echo "<label for='timeExercised'>Time spent exercising (mins): </label>";
							echo "<input type='number' id='timeExercised' name='timeExercised' min='0' value='30'><br>";
							echo "<label for='distanceTravelled'>Distance Travelled: </label>";
							echo "<input type='number'id='distanceTravelled' name='distanceTravelled' min='0' value='0' step='100'><br>";
							echo "<label for='distanceTravelled'>(metres or 0 for N/A)</label><br>";
							echo "<label for='location'>Enter the location: </label>";
							echo "<input type='text' id='location' name='location' value='N/A'><br>";
							echo "<label for='comment'>Add a comment or leave blank: </label><br>";
							echo "<textarea id='comment' name='comment' maxlength='256' cols='64' rows='4' placeholder='comment here' ></textarea><br>";
							echo "<input type='submit' name='submit' value='Submit'>";
						echo "</form>";

						mysql_free_result($result);
					}//end ShowForm()
				?>
			</div>

			<div class="footer">
			</div>
		</div>
	</body>
</html>