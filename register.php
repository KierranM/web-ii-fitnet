<!DOCTYPE html>
<html>
<head>
	<title>FitNet - Register</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href='css/fitnetstyle.css'>
	<script src='javascript/jquery.js' type="text/javascript" charset="utf-8"></script>
	<script src='javascript/register.js' type="text/javascript" charset="utf-8"></script>
	<script src='javascript/alignFormInputs.js' type="text/javascript" charset="utf-8"></script>
</head>
	<body>
		<div class="wrapper">
			<div class="header">
				<?php
					include 'connect.php';
					$select = 'SELECT FilePath FROM BannerImages ORDER BY RAND() LIMIT 1';
					$result = mysql_query($select);
					$row = mysql_fetch_assoc($result);
					$filePath = $row['FilePath'];
					echo "<img src='$filePath' alt='an image'>";
				?>
			</div>

			<div class="content">
					<?php

						if (!isset($_POST['submit'])) {
							ShowForm();
						}
						else{
							include 'connect.php';


							$first = strip_tags($_POST['first']);
							$last = strip_tags($_POST['last']);
							$email = strip_tags($_POST['email']);
							$password = strip_tags($_POST['password']);
							$repassword = strip_tags($_POST['repassword']);
							$displayName = strip_tags($_POST['displayName']);

							$errors = CountErrors($first, $last, $email, $displayName, $password, $repassword);

							if ($errors == 0) {
								$size = 128;
								$profilePic = "http://permissiondenied.net/identicon/hash/$size/$displayName.png";
								$addResult = AddUser($first, $last, $email, $password, $displayName, $profilePic);
								if ($addResult == 1) {
									ShowSuccess($first, $last);
								}
								else
								{
									ShowMySQLError();
								}
							}
							else
							{
								ShowErrorForm($first, $last, $email, $displayName, $password, $repassword);
							}
						}

						function ShowForm()
						{
							echo "<div id='registrationForm'>";
							echo "<form action='register.php' method='POST'>";

								echo "<label for='first'>First Name: </label>";
								echo "<input type='text' id='first' name='first' placeholder='George' maxlength='20'><br>";

								echo "<label for='last'>Last Name: </label>";
								echo "<input type='text' id='last' name='last' placeholder='Watson' maxlength='20'><br>";

								echo "<label for='email'>Email: </label>";
								echo "<input type='email' id='email' name='email' placeholder='george@gmail.com' maxlength='30'><br>";

								echo "<label for='password'>Password: </label>";
								echo "<input type='password' id='password' name='password' maxlength='16'><br>";

								echo "<label for='repassword'>Re-enter Password: </label>";
								echo "<input type='password' id='repassword' name='repassword' maxlength='16'><br>";

								echo "<label for='displayName'>Display Name: </label>";
								echo "<input type='text' id='displayName' name='displayName' placeholder='GeorgiePie' maxlength='15'><br>";
								echo "<br>";
								echo "<label>Your profile pic:</label>";
								echo "<img src='' id='identicon' height='128' width='128'><br>";

								echo "<input type='submit' id='registerButton' name='submit' value='Register'>";

							echo "</form>";
							echo "</div>";
							ShowWhatIsFitnetText();
						}

						function ShowSuccess($firstName, $lastName)
						{
							echo "<h3>Congratulations $firstName $lastName, you are now signed up for FitNet</h4><br>";
							echo "<b>Redirecting to login in <b id='timeleft'>3</b> seconds</b>";
						}

						function ShowMySQLError()
						{
							echo "<h3>Sorry there was an error, please try again later</h3>";
						}

						function ShowErrorForm($firstName, $lastName, $emailAddress, $displayName, $password, $repassword)
						{
							echo "<div id='registrationForm'>";
							echo "<form action='register.php' method='POST'>";

								echo "<label for='first'>First Name: </label>";
								echo "<input type='text' id='first' name='first' maxlength='20' value='$firstName'><br>";
								if (strlen($firstName) == 0) {
									echo "<b class='incorrect'>Please enter a name</b><br>";
								}

								echo "<label for='last'>Last Name: </label>";
								echo "<input type='text' id='last' name='last' maxlength='20' value='$lastName'><br>";
								if (strlen($lastName) == 0) {
									echo "<b class='incorrect'>Please enter a name</b><br>";
								}

								echo "<label for='email'>Email: </label>";
								echo "<input type='email' id='email' name='email' maxlength='30' value='$emailAddress'><br>";
								if (ValidateEmail($emailAddress) == false) {
									echo "<b class='incorrect'>Please enter a valid email</b>";
								}
								echo "<br>";
								
								echo "<label for='password'>Password: </label>";
								echo "<input type='password' id='password' name='password' maxlength='16'><br>";
								if (ValidatePassword($password) == false) {
									echo "<b class='incorrect'>Your password must contain:</b>";
									echo "<ul>";
									if (CheckPasswordLength($password) == false) {
										echo "<li class='incorrect'>At least 8 characters</li>";
									}
									if (CheckPasswordHasNumerics($password) == false) {
										echo "<li class='incorrect'>At least one number (0-9)</li>";
									}
									if (CheckPasswordHasUpperCase($password) == false) {
										echo "<li class='incorrect'>At least one uppercase letter</li>";
									}
									if (CheckPasswordHasLowerCase($password) == false) {
										echo "<li class='incorrect'>At least one lowercase letter</li>";
									}
									echo "</ul>";
								}

								echo "<label for='repassword'>Re-enter Password: </label>";
								echo "<input type='password' id='repassword' name='repassword' maxlength='16'>";
								if ($password != $repassword) {
									echo "<b class='incorrect'>Passwords didn't match</b>";
								}
								echo "<br>";

								echo "<label for='displayName'>Display Name: </label>";
								echo "<input type='text id='displayName' name='displayName' value='$displayName' maxlength='15'><br>";
								if (ValidateDisplay($displayName) == false) {
									echo "<b class='incorrect'>Invalid display name <br>(No Spaces/Symobls, at least 8 characters)</b>";
								}
								elseif(DuplicateDisplayName($displayName) == false)
								{
									echo "<b class='incorrect'>This display name has been taken</b>";
								}
								echo "<br>";
								echo "<br>";
								echo "<label>Your profile pic: </label>";
								echo "<img src='' id='identicon' height='128' width='128'>";
								
								echo "<br><input type='submit' id='registerButton' name='submit' value='Register'>";


							echo "</form>";
							echo "</div>";
							ShowWhatIsFitnetText();
						}

						function AddUser($firstName, $lastName, $emailAddress, $password, $displayName, $profilePic)
						{
							$hashed = md5($password);

							$firstName = addslashes($firstName);
							$lastName = addslashes($lastName);
							$emailAddress = addslashes($emailAddress);
							$displayName = addslashes($displayName);

							$add = "INSERT INTO Users (firstName, lastName, Password, Email, DisplayName, ProfilePicture) VALUES ('$firstName', '$lastName', '$hashed', '$emailAddress', '$displayName', '$profilePic')";

							$result = mysql_query($add);

							return $result;
						}


						function CountErrors($firstName, $lastName, $email, $displayName, $password, $repassword)
						{
							$count = 0;

							if(strlen($firstName) == 0 || strlen($lastName) == 0){
								$count++;
							}

							if (!ValidateEmail($email)) {
								$count++;
							}

							if (!ValidatePassword($password)) {
								$count++;
							}
							else
							{
								if ($password != $repassword) {
									$count++;
								}
							}

							if (!ValidateDisplay($displayName)) {
								$count++;
							}
							else
							{
								if (!DuplicateDisplayName($displayName)) {
									$count++;
								}
							}

							return $count;
						}

						function ValidateEmail($email)
						{
							$regex = "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";

							$val = preg_match("/$regex/", $email);

							return $val;
						}

						function ValidatePassword($password)
						{
							$val = false;
							if (CheckPasswordLength($password)) {
								if (CheckPasswordHasNumerics($password)) {
									if (CheckPasswordHasUpperCase($password)) {
										if (CheckPasswordHasLowerCase($password)) {
											$val = true;
										}
									}
								}
							}
							return $val;
						}

						function ValidateDisplay($displayName)
						{
							$regex = "[a-zA-Z0-9_\-]{8,16}";

							$val = preg_match("/$regex/", $displayName);

							return $val;
						}

						function DuplicateDisplayName($displayName)
						{
							$select = "SELECT * FROM Users WHERE BINARY DisplayName = '$displayName'";

							$result = mysql_query($select);

							if (mysql_num_rows($result) > 0) {
								return false;
							}
							else
							{
								return true;
							}
						}

						function CheckPasswordLength($password)
						{
							$regex = ".{8,16}";

							$val = preg_match("/$regex/", $password);

							return $val;
						}

						function CheckPasswordHasNumerics($password)
						{
							$regex = ".*[0-9]+.*";

							$val = preg_match("/$regex/", $password);

							return $val;
						}

						function CheckPasswordHasUpperCase($password)
						{
							$regex = ".*[A-Z]+";

							$val = preg_match("/$regex/", $password);

							return $val;
						}

						function CheckPasswordHasLowerCase($password)
						{
							$regex = ".*[a-z]+";

							$val = preg_match("/$regex/", $password);

							return $val;
						}

						function ShowWhatIsFitnetText()
						{
							echo "<div id='siteDescription'>
									<h2>What is FitNet?</h2>
									<p>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
									tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
									quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
									consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
									cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
									proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
								</div>";
						}
					?>
				</div>
				
			</div>

			<div class="footer">
			</div>
		</div>
	</body>
</html>