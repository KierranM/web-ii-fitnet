<?php
	include "accessControl.php";
?>
<!----
	Purpose: This is the error page that is displayed when a page cannot be displayed
-->
<!DOCTYPE html>
<html>
<head>
	<title>FitNet - File Not Found</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/fitnetstyle.css">
	<script src="javascript/jquery.js" type="text/javascript" charset="utf-8"></script>
</head>
	<body>
		<div class="wrapper">
			<div class="header">
				<?php
					//Select a random banneer to display
					include 'connect.php';
					$select = 'SELECT FilePath FROM BannerImages ORDER BY RAND() LIMIT 1';
					$result = mysql_query($select);
					$row = mysql_fetch_assoc($result);
					mysql_free_result($result);
					$filePath = $row['FilePath'];
					echo "<img src='$filePath' alt='an image'>";
				?>
			</div>

			<div class="content">
				<?php
					include 'navbar.php';
				?>

				<h2>The page you are looking for does not exist. Sorry :(</h2>
			</div>

			<div class="footer">
			</div>
		</div>
	</body>
</html>