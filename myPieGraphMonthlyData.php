<?php
	require_once("jpgraph/jpgraph.php");
	require_once("jpgraph/jpgraph_pie.php");
	include "accessControl.php";
	$myID = $_SESSION['userID'];
	$firstDayOfMonth = date('Y-m-01');
	$todaysDate = date('Y-m-d');
	$select = "SELECT a.ActivityName, a.ColourCode FROM Activity AS a JOIN Workout AS w ON w.ActivityID = a.ActivityID WHERE w.UserID = $myID AND w.Date >= '$firstDayOfMonth' GROUP BY a.ActivityID";
	
	$result = mysql_query($select);
	$legend = array();
	$colour = array();
	while ($row = mysql_fetch_assoc($result))
	{
	$legend[]= $row["ActivityName"];
	$colour[]= $row["ColourCode"]; 
	}
	
	$graph = new PieGraph (300, 200);
	$graph->SetShadow();
	$graph->title->Set("Types Of Exercise You Have Done This Month"); 
	$graph->title->SetFont(FF_ARIAL, FS_BOLD);
	$graph->title->SetColor("#43609c", "black");
	
	$select = "SELECT count(*) AS count FROM Workout WHERE UserID = $myID AND Date >= '$firstDayOfMonth' GROUP BY ActivityID";
	$result = mysql_query($select);
	if (mysql_num_rows($result) > 0) {
		
		$data = array();
		while ($row = mysql_fetch_assoc($result))
		{
		$data[] = $row["count"];
		}
		
		$p1 = new PiePlot($data);
		$p1->SetLegends($legend);
		$graph->Add($p1);
		$p1->SetSliceColors($colour);
		$p1->value->SetFont(FF_ARIAL, FS_BOLD);
		$p1->SetLabelPos(0.6);
		$p1->value->SetColor("black");
		

		
		$graph->Stroke();
	}
	else{
		$im = imagecreate(210, 30);

		// White background and blue text
		$bg = imagecolorallocate($im, 255, 255, 255);
		$textcolor = imagecolorallocate($im, 0, 0, 255);

		// Write the string at the top left
		imagestring($im, 5, 0, 0, 'No Exercises this month', $textcolor);

		// Output the image
		header('Content-type: image/png');

		imagepng($im);
		imagedestroy($im);
	}
?>