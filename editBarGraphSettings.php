<?php
	include "accessControl.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>FitNet - Edit Bar Graph Settings</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/fitnetstyle.css">
	<script src="javascript/jquery.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src='javascript/alignFormInputs.js'></script>
	<script type="text/javascript" src='javascript/selectLimiter.js'></script>
</head>
	<body>
		<div class="wrapper">
			<div class="header">
				<?php
					include 'connect.php';
					$select = 'SELECT FilePath FROM BannerImages ORDER BY RAND() LIMIT 1';
					$result = mysql_query($select);
					$row = mysql_fetch_assoc($result);
					mysql_free_result($result);
					$filePath = $row['FilePath'];
					echo "<img src='$filePath' alt='an image'>";
				?>
			</div>

			<div class="content">
				<?php
					include 'navbar.php';
					include 'profileNav.php';
					$myID = $_SESSION['userID'];
					if (isset($_POST['submit'])) {
						//Dump old friends
						$delete = "DELETE FROM GraphFriendSettings WHERE UserID = $myID";
						$result = mysql_query($delete);

						//Get the friends to add
						$friendsToAdd = array();
						$friendsToAdd[] = strip_tags($_POST['friendOne']);
						$friendsToAdd[] = strip_tags($_POST['friendTwo']);
						$friendsToAdd[] = strip_tags($_POST['friendThree']);
						$friendsToAdd[] = strip_tags($_POST['friendFour']);
						$friendsToAdd[] = strip_tags($_POST['friendFive']);
						$success = true;
						foreach ($friendsToAdd as $key => $value) {
							if ($value != 'default') {
								//Add the friend to your settings
								$insert = "INSERT INTO GraphFriendSettings (UserID, GraphFriendID) VALUES ($myID,$value)";
								$result = mysql_query($insert);

								if ($result == false) {
									$success = false;
								}
							}
						}

						if ($success) {
							echo "<h2>Your settings updated correctly</h2>";
						}
						else{
							echo "<h2>Something went wrong, please try again</h2>";
						}
					}
					echo "<br>";
					ShowForm($myID);

					function ShowForm($myID)
					{
						echo "<h2 class='noPadding noMargin'>My Graph Friends</h2>";
						echo "<hr>";

						$selectMyFriends = "SELECT u.FirstName, u.LastName, u.UserID FROM Users AS u JOIN Friends AS f ON u.UserID = f.UserID WHERE f.FriendID = $myID AND f.Accepted = TRUE";
						$result = mysql_query($selectMyFriends);
						$friends = array();
						while ($row = mysql_fetch_assoc($result)) {
							$friends[] = $row;
						}

						echo "<form action='editBarGraphSettings.php' method='POST'>";
							echo "<label for='friendOne'>Friend One: </label>";
							echo "<select id'friendOne' name='friendOne'>";
								echo "<option value='default'>No One</option>";
								$graphFriend = SelectGoalFriend(0,$myID);
								foreach ($friends as $value) {
									var_dump($graphFriend);
									var_dump($value);
									echo "<option value='".$value['UserID']."' ";
									
									if (!empty($graphFriend) && $value['UserID'] == $graphFriend['GraphFriendID']) {
										echo "selected";
									}
									echo ">".$value['FirstName'] . " " . $value['LastName']. "</option>";
								}
							echo "</select><br>";

							echo "<label for='friendTwo'>Friend Two: </label>";
							echo "<select id'friendTwo' name='friendTwo'>";
								echo "<option value='default'>No One</option>";
								$graphFriend = SelectGoalFriend(1,$myID);
								foreach ($friends as $value) {
									var_dump($graphFriend);
									var_dump($value);
									echo "<option value='".$value['UserID']."' ";
									
									if (!empty($graphFriend) && $value['UserID'] == $graphFriend['GraphFriendID']) {
										echo "selected";
									}
									echo ">".$value['FirstName'] . " " . $value['LastName']. "</option>";
								}
							echo "</select><br>";

							echo "<label for='friendThree'>Friend Three: </label>";
							echo "<select id'friendThree' name='friendThree'>";
								echo "<option value='default'>No One</option>";
								$graphFriend = SelectGoalFriend(2,$myID);
								foreach ($friends as $value) {
									echo "<option value='".$value['UserID']."' ";
									if (!empty($graphFriend) && $value['UserID'] == $graphFriend['GraphFriendID']) {
										echo "selected";
									}
									echo ">".$value['FirstName'] . " " . $value['LastName']. "</option>";
								}
							echo "</select><br>";

							echo "<label for='friendFour'>Friend Four: </label>";
							echo "<select id'friendFour' name='friendFour'>";
								echo "<option value='default'>No One</option>";
								$graphFriend = SelectGoalFriend(3,$myID);
								foreach ($friends as $value) {
									echo "<option value='".$value['UserID']."' ";
									if (!empty($graphFriend) && $value['UserID'] == $graphFriend['GraphFriendID']) {
										echo "selected";
									}
									echo ">".$value['FirstName'] . " " . $value['LastName']. "</option>";
								}
							echo "</select><br>";

							echo "<label for='friendFive'>Friend Five: </label>";
							echo "<select id'friendFive' name='friendFive'>";
								echo "<option value='default'>No One</option>";
								$graphFriend = SelectGoalFriend(4,$myID);
								foreach ($friends as $value) {
									echo "<option value='".$value['UserID']."' ";
									if (!empty($graphFriend) && $value['UserID'] == $graphFriend['GraphFriendID']) {
										echo "selected";
									}
									echo ">".$value['FirstName'] . " " . $value['LastName']. "</option>";
								}
							echo "</select><br>";

							echo "<br><input type='submit' name='submit' value='Update Graph Settings'>";
						echo "</form>";
					}

					function SelectGoalFriend($num, $myID)
					{
						$select = "SELECT * FROM GraphFriendSettings WHERE UserID = $myID LIMIT $num, 1";
						$result = mysql_query($select);
						$row = mysql_fetch_assoc($result);
						mysql_free_result($result);
						return $row;
					}
				?>
			</div>

			<div class="footer">
			</div>
		</div>
	</body>
</html>