<?php
require_once ('jpgraph/jpgraph.php');
require_once ('jpgraph/jpgraph_bar.php');
include "accessControl.php";
include "connect.php";

$myData=array();
$friend1=array();
$friend2=array();
$friend3=array();
$friend4=array();
$friend5=array();
$myID = $_SESSION['userID'];
$mydata = getTotalMinutesForUser($myID);
$select = ("SELECT gf.GraphFriendID, u.FirstName, u.LastName FROM GraphFriendSettings AS gf JOIN Users AS u ON gf.GraphFriendID = u.UserID WHERE gf.UserID = $myID");
$result = mysql_query($select);
$array = array();
for($i = 0; $i < 5; $i++)
{
	$temp = mysql_fetch_assoc($result);
	if($temp != false)
	{
		$array[$i] = $temp;
	}
	else
	{
		$array[$i] = 0;
	}
}

if($array[0] != 0)
{
	$friend1 = getTotalMinutesForUser($array[0]['GraphFriendID']);
}
if($array[1] != 0)
{
	$friend2 = getTotalMinutesForUser($array[1]['GraphFriendID']);
}
if($array[2] != 0)
{
	$friend3 = getTotalMinutesForUser($array[2]['GraphFriendID']);
}
if($array[3] != 0)
{
	$friend4 = getTotalMinutesForUser($array[3]['GraphFriendID']);
}
if($array[4] != 0)
{
	$friend5 = getTotalMinutesForUser($array[4]['GraphFriendID']);
}

// Create the graph. These two calls are always required
$graph = new Graph(350,200,'auto');
$graph->SetScale("textlin");

$theme_class=new UniversalTheme;
$graph->SetTheme($theme_class);

$graph->SetBox(false);
$graph->ygrid->SetFill(false);
$graph->xaxis->SetTickLabels(array('Mon','Tues','Wed','Thurs', 'Fri', 'Sat', 'Sun'));
$graph->yaxis->HideLine(false);
$graph->yaxis->HideTicks(false,false);

// Create the bar plots
$myPlot = new BarPlot($mydata);
$myPlot->SetLegend('Me');
$groupPlots = array();
$groupPlots[] = $myPlot;

if(!empty($friend1))
{
	$temp = new BarPlot($friend1);
	$temp->SetLegend($array[0]['FirstName'].' '.$array[0]['LastName']);
	$groupPlots[] = $temp;
}
if(!empty($friend2))
{
	$temp = new BarPlot($friend2);
	$temp->SetLegend($array[1]['FirstName'].' '.$array[1]['LastName']);
	$groupPlots[] = $temp;
}
if(!empty($friend3))
{
	$temp = new BarPlot($friend3);
	$temp->SetLegend($array[2]['FirstName'].' '.$array[2]['LastName']);
	$groupPlots[] = $temp;
}
if(!empty($friend4))
{
	$temp = new BarPlot($friend4);
	$temp->SetLegend($array[3]['FirstName'].' '.$array[3]['LastName']);
	$groupPlots[] = $temp;
}
if(!empty($friend5))
{
	$temp = new BarPlot($friend5);
	$temp->SetLegend($array[4]['FirstName'].' '.$array[4]['LastName']);
	$groupPlots[] = $temp;
}

$graph->xaxis->SetTitle('Day Of Week', 'center');
$graph->yaxis->SetTitle('Minutes Exercised', 'center');
// Create the grouped bar plot
$gbplot = new GroupBarPlot($groupPlots);
// ...and add it to the graPH
$graph->Add($gbplot);

$graph->title->Set("You And Your Friend's Total Weekly Time Comparrison");
$graph->title->SetFont(FF_ARIAL, FS_BOLD);
$graph->title->SetColor("#43609c", "black");

// Display the graph
$graph->Stroke();


function getTotalMinutesForUser($userID)
{
$monday = strtotime('last monday', strtotime('tomorrow'));

$array = array();
 for($i = 0; $i < 7; $i++)
 {
	$temp = mktime(0, 0, 0, date("m", $monday), date("d", $monday)+$i, date("Y", $monday));
	$date = date("Y-m-d", $temp);
	$select = "SELECT SUM(TotalTime) FROM Workout WHERE UserID = $userID AND Date = '$date' GROUP BY Date";
	$result = mysql_query($select);
	$value = 0;
	if(mysql_num_rows($result)> 0)
	{
		$row = mysql_fetch_row($result);
		$value = intval($row[0]);
	}
	$array[] = $value;
 }
 
return $array;

}
