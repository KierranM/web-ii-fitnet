<?php
require_once ('jpgraph/jpgraph.php');
require_once ('jpgraph/jpgraph_bar.php');
include "accessControl.php";
include "connect.php";

if(isset($_GET['id']))
{
	$friendID = strip_tags($_GET['id']);
	$myID = $_SESSION['userID'];
	$checkIfFriend = "SELECT f.Accepted FROM Users AS u JOIN Friends AS f ON u.UserID = f.FriendID WHERE f.FriendID = $friendID AND f.UserID = $myID";
	$result = mysql_query($checkIfFriend);
	$row = mysql_fetch_assoc($result);
	mysql_free_result($result);
	$accepted = (bool)$row['Accepted'];
	
	if($accepted)
	{
		$myData = getTotalMinutesForUser($myID);
		$friend = getTotalMinutesForUser($friendID);
		$select = ("SELECT FirstName, LastName FROM Users WHERE UserID = $friendID");
		$result = mysql_query($select);
		$row = mysql_fetch_assoc($result);
		// Create the graph. These two calls are always required
		$graph = new Graph(350,200,'auto');
		$graph->SetScale("textlin");

		$theme_class=new UniversalTheme;
		$graph->SetTheme($theme_class);

		$graph->SetBox(false);
		$graph->ygrid->SetFill(false);
		$graph->xaxis->SetTickLabels(array('Mon','Tues','Wed','Thurs', 'Fri', 'Sat', 'Sun'));
		$graph->yaxis->HideLine(false);
		$graph->yaxis->HideTicks(false,false);

		$myPlot = new BarPlot($myData);
		$myPlot->SetLegend('Me');
		$friendPlot = new BarPlot($friend);
		$friendPlot->SetLegend($row['FirstName'].' '.$row['LastName']);
		$graph->xaxis->SetTitle('Day Of Week', 'center');
		$graph->yaxis->SetTitle('Minutes Exercised', 'center');
		// Create the grouped bar plot
		$gbplot = new GroupBarPlot(array($myPlot, $friendPlot));
		// ...and add it to the graPH
		$graph->Add($gbplot);

		$graph->title->Set("You And Your Friend's Total Weekly Time Comparrison");
		$graph->title->SetFont(FF_ARIAL, FS_BOLD);
		$graph->title->SetColor("#43609c", "black");

		// Display the graph
		$graph->Stroke();
	}
	
	
}

function getTotalMinutesForUser($userID)
{
$monday = strtotime('last monday', strtotime('tomorrow'));

$array = array();
 for($i = 0; $i < 7; $i++)
 {
	$temp = mktime(0, 0, 0, date("m", $monday), date("d", $monday)+$i, date("Y", $monday));
	$date = date("Y-m-d", $temp);
	$select = "SELECT SUM(TotalTime) FROM Workout WHERE UserID = $userID AND Date = '$date' GROUP BY Date";
	$result = mysql_query($select);
	$value = 0;
	if(mysql_num_rows($result)> 0)
	{
		$row = mysql_fetch_row($result);
		$value = intval($row[0]);
	}
	$array[] = $value;
 }
 
return $array;

}