<?php 
$currentPage = basename($_SERVER['PHP_SELF']);
echo "<nav class='sub'>";
	if($currentPage != "myProfile.php"){
		echo "<a class='links subLink navBoxLeftEdge' href='myProfile.php'>My Profile</a>";
	}
	else{
		echo "<b class='links subLink current navBoxLeftEdge'>My Profile</b>";
	}
	if($currentPage != "allMyData.php"){
		echo "<a class='links subLink navBoxCenter' href='allMyData.php'>All My Data</a>";
	}
	else{
		echo "<b class='links subLink current navBoxCenter'>All My Data</b>";
	}
	if($currentPage != "myRawData.php"){
		echo "<a class='links subLink navBoxRightEdge' href='myRawData.php'>My Raw Data</a>";
	}
	else{
		echo "<b class='links subLink current navBoxRightEdge'>My Raw Data</b>";
	}
	
echo "</nav><br><br>";