<?php
	include "accessControl.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>FitNet - My Profile</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href='css/fitnetstyle.css'>
	<script src='javascript/jquery.js' type="text/javascript" charset="utf-8"></script>
	<script src='javascript/evenOutProfilePic.js' type="text/javascript" charset="utf-8"></script>
</head>
	<body>
		<div class="wrapper">
			<div class="header">
			<?php
					include 'connect.php';
					$select = 'SELECT FilePath FROM BannerImages ORDER BY RAND() LIMIT 1';
					$result = mysql_query($select);
					$row = mysql_fetch_assoc($result);
					mysql_free_result($result);
					$filePath = $row['FilePath'];
					echo "<img src='$filePath' alt='an image'>";
				?>
			</div>

			<div class="content">
				<?php
					error_reporting(E_ERROR);
					include "navbar.php";
					include 'profileNav.php';

					if (isset($_POST['like'])) {
						$IDofWorkoutToLike = $_POST['workoutID'];
						$myID = $_SESSION['userID'];
						$updateValue = "INSERT INTO WorkoutLikes (WorkoutID, LikerID) VALUES ($IDofWorkoutToLike,$myID)";
						$result = mysql_query($updateValue);
					}
					elseif (isset($_POST['unlike'])) {
						$IDofWorkoutToUnlike = $_POST['workoutID'];
						$myID = $_SESSION['userID'];
						$delete = "DELETE FROM WorkoutLikes WHERE WorkoutID = $IDofWorkoutToUnlike AND LikerID = $myID";
						$result = mysql_query($delete);
					}

					GetProfile($_SESSION['userID']);
					echo "<br>";
					ShowMonthlyPieGraph();
					ShowWeeklyTimeComparrisonGraph();
					echo "<br>";
					GetHistory($_SESSION['userID']);

					
					function GetProfile($uid)
					{
					$selectName = 
						"(	
							SELECT u.FirstName, u.LastName, u.LastExercised, u.ProfilePicture, u.DisplayName
							FROM Users as u
							WHERE u.UserID = $uid
						)";
						$result = mysql_query($selectName);
						ShowProfle(mysql_fetch_assoc($result));
						mysql_free_result($result);

					}

					function GetHistory($uid)
					{
						echo "<h2 class='noPadding noMargin'>Workout History</h2>";
						echo "<hr>";
						$select = "	SELECT u.DisplayName, u.ProfilePicture, w.WorkoutID, w.Comment, w.TotalTime, w.Distance,w.Date, w.Added, w.Area, a.ActivityName 
									FROM Activity as a JOIN Workout as w
								   	ON a.ActivityID = w.ActivityID
									JOIN Users as u 
									ON u.UserID = w.UserID 
									WHERE u.UserID = $uid
									ORDER BY w.Date DESC,w.Added DESC";

						$result = mysql_query($select);

						if ($result > 0) {
							echo "<div class='workoutsContainer'>";
								while ($row = mysql_fetch_assoc($result)) {
									$workoutID = $row['WorkoutID'];
									$selectLikes = "SELECT COUNT(*) AS LikeCount FROM WorkoutLikes WHERE WorkoutID = $workoutID";
									$rs = mysql_query($selectLikes);
									$rw = mysql_fetch_assoc($rs);
									$likeCount = $rw['LikeCount'];
									ShowWorkout($row, $likeCount);
								}
							echo "</div>";
						}
						else {
							echo "<h4>No exercise history>/h4>";
						}
						
					}
					
					function ShowMonthlyPieGraph()
					{
						echo("<img src = 'myPieGraphMonthlyData.php' alt='Pie Graph'>");
					}
					
					function ShowWeeklyTimeComparrisonGraph()
					{
						echo("<img class='floatright' src ='myWeeklyTimeComparrison.php' alt='Weekly Comparision'><br>");
						echo "<a class='floatright' href='editBarGraphSettings.php'>Click here to edit your bar graph settings</a>";
					}


					function ShowWorkout($values, $likeCount)
					{
						$displayName = $values['DisplayName'];
						$profilePic = $values['ProfilePicture'];
						$comment = $values['Comment'];
						$likes = $likeCount;
						$date = $values['Date'];
						$workoutID = $values['WorkoutID'];
						$activityName = $values['ActivityName'];
						$timeTaken = round($values['TotalTime']/60,2);
						$distance = round($values['Distance']/1000,2);
						$area = $values['Area'];

						$d = strtotime($date);
						$betterDate = date("F j, Y",  $d);

						echo "<form action='myProfile.php' method='POST'>";
							echo "<div class='homeWorkout box'>";
								echo "<div class='profilePic'>";
									echo "<img src='$profilePic' height='32' width='32 alt='Profile Pic'>";
								echo "</div>";
								echo "<b>$displayName</b><br>";
								echo "<p class='workoutComment'>$comment <i class='smallText'> + Activity: $activityName, Time: $timeTaken hrs";
								if ($area != "N/A") {
									echo ", Location: $area";
								}
								if ($distance != "N/A") {
									echo ", Distance: $distance km";
								}
								echo "</i></p>";
								echo "<i class='datesNewsFeed'>On $betterDate</i>";
								echo "<i class='workoutInfo datesNewsFeed'>Likes: $likes</i>";
								echo "<input type='hidden' name='workoutID' value='$workoutID'>";
								if (HasUserLikedWorkout($workoutID, $_SESSION['userID'])) {
									echo "<input type='submit' name='unlike' value='Unlike' class='datesNewsFeed linkButton'>";
								}
								else{
									echo "<input type='submit' name='like' value='Like' class='datesNewsFeed linkButton'>";
								}
							echo "</div>";
						echo "</form>";
					}

					function HasUserLikedWorkout($workoutID, $userID)
					{
						$select = "SELECT * FROM WorkoutLikes WHERE WorkoutID = $workoutID AND LikerID = $userID";
						$result = mysql_query($select);
						$numRows = mysql_num_rows($result);
						if ($numRows > 0) {
							return true;
						}
						else{
							return false;
						}
					}
					
					function ShowProfle($values)
					{
						echo "<h2 class='noPadding noMargin'>My Profile</h2>";
						echo "<hr>";
						$firstName = $values['FirstName'];
						$lastName = $values['LastName'];
						$displayName = $values['DisplayName'];
						$lastExercised = $values['LastExercised'];

						//Change the last exercised into a nicer format
						$temp = strtotime($lastExercised);
						$niceDate = date("F j, Y",  $temp);
						if ($niceDate == "January 1, 1970") {
							$niceDate = "Never";
						}

						$profilePicture = $values['ProfilePicture'];

						echo "<div class='profile'>";
							echo "<div class='profilePic'>";
								echo "<img src='$profilePicture' height='128' width='128' alt='Profile Picture'>";
							echo "</div>";
							echo "<b>$firstName $lastName ($displayName)</b><br>";
							echo "<b class='smallText'>Last Exercised: $niceDate</b><br>";
							echo "<a class='smallText' href='editProfilePic.php'>Change profile picture</a>";
						echo "</div>";

					}
				?>
			</div>

			<div class="footer">
			</div>
		</div>
	</body>
</html>