jQuery(document).ready(function($) {
	$("[name='displayName']").keypress(function(event) {
		var displayName = $(this).val();
		var size = 128;
		$('#identicon').attr('src', 'http://permissiondenied.net/identicon/hash/'+size+'/'+displayName+'.png');
	});

	var success = $('#timeleft');
	if (success.length !== 0) {
		window.setInterval(function() {
			var timeleft = success.html();
			if (timeleft <= 0) {
				window.location= ("login.php");                 
        	}else{              
            	success.html(timeleft-1)
        	}
    	}, 1000); 
	}
});