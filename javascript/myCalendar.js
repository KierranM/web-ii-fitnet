$(document).ready(function() {
      
	var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();
	
	$('#calendar').fullCalendar({
		editable: false,
		events: {
       		url: 'events.php',
        	type: 'GET'
    	}
	});             
});