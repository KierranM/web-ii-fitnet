jQuery(document).ready(function($) {
	$('#goaltype').change(function(event) {
		var options = {
			'total': 1,
			'duration': 2,
			'distance': 3,
			'avgDuration': 4,
			'avgDistance': 5
		}
		var values = {
			'total': 'workouts',
			'duration': 'total minutes',
			'distance': 'total metres',
			'avgDuration': 'avg. minutes',
			'avgDistance': 'avg. metres'
		}
		var stepSizes = {
			'total': 1,
			'duration': 60,
			'distance': 500,
			'avgDuration': 30,
			'avgDistance': 100
		}
		var goaltype = parseInt($(this).val());
		var messageLabel = $('#valueTypeDisplay');
		var numberTicker = $('#goalvalue');
		switch(goaltype)
		{
			case options.total:
				messageLabel.text(values.total);
				numberTicker.attr('step', stepSizes.total);
				break;
			case options.duration:
				messageLabel.text(values.duration);
				numberTicker.attr('step', stepSizes.duration);
				break;
			case options.distance:
				messageLabel.text(values.distance);
				numberTicker.attr('step', stepSizes.distance);
				break;
			case options.avgDuration:
				messageLabel.text(values.avgDuration);
				numberTicker.attr('step', stepSizes.avgDuration);
				break;
			case options.avgDistance:
				messageLabel.text(values.avgDistance);
				numberTicker.attr('step', stepSizes.avgDistance);
				break;
		}
	});
});