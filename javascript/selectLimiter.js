jQuery(document).ready(function($) {
	var previous;
	$('select').focus(function(event) {
		previous = $(this).val();
	}).change(function(event) {
		var current = this;
		$('select').each(function(index, el) {
			if(el !== current)
			{
				$(el).children('option').each(function(index, child) {
					if($(child).val() === previous)
					{
						$(child).show();
					}
				});
				$(el).children('option').each(function(index, child) {
					if (($(child).val() === $(current).val()) && ($(child).val() !== 'default')) {
						$(child).hide();
					};
				});
			}
		});
	});


	$('select').each(function(index, el) {
		var current = el;
		$('select').each(function(index, child) {
			if(child !== current)
			{
				$(child).children('option').each(function(index, grandchild) {
					if (($(grandchild).val() === $(current).val()) && ($(grandchild).val() !== 'default')) {
						$(grandchild).hide();
					};
				});
			}
		});
	});
});