jQuery(document).ready(function($) {
	$("form").submit(function(event) {
		var valid;
		var file = $('#choice').val();
		if (file !== "") {
			var extension = file.split('.').pop().toLowerCase();
			switch(extension)
			{
				case "png":
				case "jpeg":
				case "gif":
				case "jpg":
					valid = true;
					break;
				default:
					valid = false;
					break;
			}
		}
		else{
			valid = false;
		}

		if (!valid) {
			$('#warning').show().fadeOut('500');
			event.preventDefault();
		}
	});
});