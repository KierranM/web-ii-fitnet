jQuery(document).ready(function($) {
	$("[name='activity']").change(function(event) {
		var value = $(this).val();
		if (value === 'newActivity') {
			$('.newActivity').slideDown();
		}
		else
		{
			$('.newActivity').slideUp();
		}
	});

	$('#datepicker').keypress(function(event) {
		event.preventDefault();
	});

	
	$('form').submit(function(event) {
		var selectedActivity = $('#activity').val();

		if (selectedActivity === 'newActivity') {
			var newActivityName = $('#activityName').val();
			if (newActivityName === "") {
				$('#warning').show().fadeOut(500);
				event.preventDefault();
			};
		};
	});
});