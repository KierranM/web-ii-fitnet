jQuery(document).ready(function($) {

	$('form').each(function(index, el) {
		var largestPosition = 0;
		
		$(this).children('label').each(function(index, el) {
			var elPos = $(el).offset();
			var elementWidth = $(el).width();
			if (elPos.left + elementWidth > largestPosition) {
				largestPosition = elPos.left + elementWidth
			};

		});

		$(this).children('input,select,img').each(function(index, el) {
			var topPos = $(el).offset().top;
			var elementWidth = $(el).width();
			if (!$(el).hasClass('dontAlign')) {
				if ($(el).prop('tagName') == 'select') {
					$(el).offset({top: topPos, left: largestPosition + elementWidth});
				}else{
					$(el).offset({top: topPos, left: largestPosition});
				}
			};
		});

		$(this).children('label').each(function(index, el) {
			var elementWidth = $(el).width();
			var elTop = $(el).offset().top;
			var left = largestPosition;
			$(el).offset({top: elTop, left: left - elementWidth - 5});
		});
	});
	
});