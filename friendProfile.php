<?php
	include "accessControl.php";
	include 'connect.php';

	$friendID = strip_tags($_GET['id']);
	if (is_numeric($friendID)) {
		if ($friendID > 0) {
			$select = "SELECT DisplayName FROM Users WHERE UserID = $friendID";
			$result = mysql_query($select);
			if (mysql_num_rows($result) > 0) {
				$row = mysql_fetch_assoc($result);
				$displayName = $row['DisplayName'];
				$pageName = "FitNet - " .$displayName . "'s Profile";
				mysql_free_result($result);
			}
			else{
				header("location: HTTP/1.1 404 Not Found");
			}
		}
		else{
			header("location: HTTP/1.1 404 Not Found");
		}
	}
	else{
		header("location: HTTP/1.1 404 Not Found");
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $pageName;?></title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/fitnetstyle.css">
	<script src="javascript/jquery.js" type="text/javascript" charset="utf-8"></script>
	<script src="javascript/evenOutProfilePic.js" type="text/javascript" charset="utf-8"></script>
</head>
	<body>
		<div class="wrapper">
			<div class="header">
				<?php
					include 'connect.php';
					$select = 'SELECT FilePath FROM BannerImages ORDER BY RAND() LIMIT 1';
					$result = mysql_query($select);
					$row = mysql_fetch_assoc($result);
					mysql_free_result($result);
					$filePath = $row['FilePath'];
					echo "<img src='$filePath' alt='an image'>";
				?>
			</div>

			<div class="content">
				<?php
					include 'navbar.php';
					$myID = $_SESSION['userID'];
					if (isset($_POST['accept'])) {
						$update = "UPDATE Friends SET Accepted = TRUE WHERE UserID = $friendID AND FriendID = $myID";
						$insert = "INSERT INTO Friends (UserID, FriendID, Accepted) VALUES ($myID, $friendID, TRUE)";
						$result = mysql_query($update);
						$result = mysql_query($insert);
					}
					elseif (isset($_POST['unfriend']) || isset($_POST['decline'])) {

						$delete = "DELETE FROM Friends WHERE UserID = $myID AND FriendID = $friendID";
						$deleteTwo = "DELETE FROM Friends WHERE UserID = $friendID AND FriendID = $myID";

						$result = mysql_query($delete);
						$result = mysql_query($deleteTwo);
					}
					elseif (isset($_POST['friend'])) {
						$insert = "INSERT INTO Friends (FriendID, UserID) VALUES ($friendID,$myID)";
						$result = mysql_query($insert);
					}
					elseif (isset($_POST['like'])) {
						$IDofWorkoutToLike = $_POST['workoutID'];
						$myID = $_SESSION['userID'];

						$updateValue = "INSERT INTO WorkoutLikes (WorkoutID, LikerID) VALUES ($IDofWorkoutToLike,$myID)";
						$result = mysql_query($updateValue);
					}
					elseif (isset($_POST['unlike'])) {
						$IDofWorkoutToUnlike = $_POST['workoutID'];
						$myID = $_SESSION['userID'];
						$delete = "DELETE FROM WorkoutLikes WHERE WorkoutID = $IDofWorkoutToUnlike AND LikerID = $myID";
						$result = mysql_query($delete);
					}
					echo "<br>";
					
					$checkIfFriend = "SELECT f.Accepted FROM Users AS u JOIN Friends AS f ON u.UserID = f.FriendID WHERE f.FriendID = $friendID AND f.UserID = $myID";
					$result = mysql_query($checkIfFriend);
					$row = mysql_fetch_assoc($result);
					mysql_free_result($result);
					$accepted = (bool)$row['Accepted'];
					if ($accepted) {
						ShowFriendsProfile($friendID, $myID);
					}
					else{
						ShowNonFriendProfile($friendID, $myID);
					}

					function ShowFriendsProfile($friendID, $myID)
					{
						GetProfile($friendID, $myID, true);
						echo ("<img src= 'friendWeeklyTimeComparrison.php?id=$friendID'>");
						GetHistory($friendID);
					}

					function ShowNonFriendProfile($friendID, $myID)
					{
						GetProfile($friendID, $myID, false);
					}

					function ShowProfle($values, $myID, $friend)
					{
						$firstName = $values['FirstName'];
						$lastName = $values['LastName'];
						$displayName = $values['DisplayName'];
						$lastExercised = $values['LastExercised'];
						$userID = $values['UserID'];
						echo "<h2 class='noPadding noMargin'>".$displayName."'s Profile</h2>";
						echo "<hr>";
						

						//Change the last exercised into a nicer format
						$temp = strtotime($lastExercised);
						$niceDate = date("F j, Y",  $temp);
						if ($niceDate == "January 1, 1970") {
							$niceDate = "Never";
						}

						$profilePicture = $values['ProfilePicture'];

						echo "<div class='profile'>";
							echo "<div class='profilePic'>";
								echo "<img src='$profilePicture' height='128' width='128'>";
							echo "</div>";
							echo "<b>$firstName $lastName ($displayName)</b><br>";
							if($friend) {
								echo "<b class='smallText'>Last Exercised: $niceDate</b><br>";
								echo "<form action='friendProfile.php?id=$userID' method='POST'>";
								echo "<input type='submit' name='unfriend' value='Unfriend'>";
								echo "</form>";
							}
							else{
								$hasRequestedMe = "SELECT * FROM Friends WHERE FriendID = $myID AND UserID = $userID AND Accepted = FALSE";
								$result = mysql_query($hasRequestedMe);
								if (mysql_num_rows($result) > 0) {
									echo "<form action='friendProfile.php?id=$userID' method='POST'>";
									echo "<input type='submit' name='accept' value='Accept'>";
									echo "<input type='submit' name='decline' value='Decline'>";
									echo "</form>";
								}
								else{
									$isRequested = "SELECT * FROM Friends WHERE FriendID = $userID AND UserID = $myID AND Accepted = FALSE";
									$result = mysql_query($isRequested);
									if (mysql_num_rows($result) == 0) {
										echo "<form action='friendProfile.php?id=$userID' method='POST'>";
										echo "<input type='submit' name='friend' value='Add As Friend'>";
										echo "</form>";
									}
									else{
										echo "<b>Friend invite pending</b>";
									}
								}
								
							}
						echo "</div>";

					}


					function GetProfile($uid, $myID,$friend)
					{
					$selectName = 
						"(	
							SELECT u.UserID, u.FirstName, u.LastName, u.LastExercised, u.ProfilePicture, u.DisplayName
							FROM Users as u
							WHERE u.UserID = $uid
						)";
						$result = mysql_query($selectName);
						ShowProfle(mysql_fetch_assoc($result), $myID, $friend);
						mysql_free_result($result);

					}

					function GetHistory($uid)
					{
						echo "<h2 class='noPadding noMargin'>Workout History</h2>";
						echo "<hr>";
						$select = "	SELECT u.UserID, u.DisplayName, u.ProfilePicture, w.WorkoutID, w.Comment, w.TotalTime, w.Distance,w.Date, w.Added,w.Area, a.ActivityName 
									FROM Activity as a JOIN Workout as w
								   	ON a.ActivityID = w.ActivityID
									JOIN Users as u 
									ON u.UserID = w.UserID 
									WHERE u.UserID = $uid
									ORDER BY w.Date DESC,w.Added DESC";

						$result = mysql_query($select);

						if ($result > 0) {
							echo "<div class='workoutsContainer'>";
								while ($row = mysql_fetch_assoc($result)) {
									$workoutID = $row['WorkoutID'];
									$selectLikes = "SELECT COUNT(*) AS LikeCount FROM WorkoutLikes WHERE WorkoutID = $workoutID";
									$rs = mysql_query($selectLikes);
									$rw = mysql_fetch_assoc($rs);
									$likeCount = $rw['LikeCount'];
									ShowWorkout($row, $likeCount);
								}
							echo "</div>";
						}
						else {
							echo "<h4>No exercise history>/h4>";
						}
						
					}

					function ShowWorkout($values, $likeCount)
					{
						$displayName = $values['DisplayName'];
						$profilePic = $values['ProfilePicture'];
						$comment = $values['Comment'];
						$likes = $likeCount;
						$date = $values['Date'];
						$workoutID = $values['WorkoutID'];
						$userID = $values['UserID'];
						$activityName = $values['ActivityName'];
						$timeTaken = round($values['TotalTime']/60,2);
						$distance = round($values['Distance']/1000,2);
						$area = $values['Area'];

						$d = strtotime($date);
						$betterDate = date("F j, Y",  $d);

						echo "<form action='friendProfile.php?id=$userID' method='POST'>";
							echo "<div class='homeWorkout box'>";
								echo "<div class='profilePic'>";
									echo "<img src='$profilePic' height='32' width='32'>";
								echo "</div>";
								echo "<b>$displayName</b><br>";
								echo "<p class='workoutComment'>$comment <i class='smallText'> + Activity: $activityName, Time: $timeTaken hrs";
								if ($area != "N/A") {
									echo ", Location: $area";
								}
								if ($distance != "N/A") {
									echo ", Distance: $distance km";
								}
								echo "</i></p>";
								echo "<i class='datesNewsFeed'>On $betterDate</i>";
								echo "<i class='workoutInfo datesNewsFeed'>Likes: $likes</i>";
								echo "<input type='hidden' name='workoutID' value='$workoutID'>";
								if (HasUserLikedWorkout($workoutID, $_SESSION['userID'])) {
									echo "<input type='submit' name='unlike' value='Unlike' class='datesNewsFeed linkButton'>";
								}
								else{
									echo "<input type='submit' name='like' value='Like' class='datesNewsFeed linkButton'>";
								}
							echo "</div>";
						echo "</form>";
					}

					function HasUserLikedWorkout($workoutID, $userID)
					{
						$select = "SELECT * FROM WorkoutLikes WHERE WorkoutID = $workoutID AND LikerID = $userID";
						$result = mysql_query($select);
						$numRows = mysql_num_rows($result);
						if ($numRows > 0) {
							return true;
						}
						else{
							return false;
						}
					}
				?>
			</div>

			<div class="footer">
			</div>
		</div>
	</body>
</html>