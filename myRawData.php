<?php
	include "accessControl.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Page Title</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/fitnetstyle.css">
	<script src="javascript/jquery.js" type="text/javascript" charset="utf-8"></script>
</head>
	<body>
		<div class="wrapper">
			<div class="header">
				<?php
					include 'connect.php';
					$select = 'SELECT FilePath FROM BannerImages ORDER BY RAND() LIMIT 1';
					$result = mysql_query($select);
					$row = mysql_fetch_assoc($result);
					mysql_free_result($result);
					$filePath = $row['FilePath'];
					echo "<img src='$filePath' alt='an image'>";
				?>
			</div>

			<div class="content">
				<?php
					include 'navbar.php';
					include 'profileNav.php';
					$myID = $_SESSION['userID'];
					ShowRawData($myID);
					
					function ShowRawData($myID)
					{
						echo "<h2 class='noPadding noMargin'>My Raw Data</h2>";
						echo "<hr>";
						$select = "SELECT a.ActivityName, w.* FROM Activity AS a JOIN Workout AS w ON w.ActivityID = a.ActivityID WHERE w.UserID = $myID";
						$result = mysql_query($select);

						$totalData = mysql_num_rows($result);
						echo "<table class = 'bordered'>";
						echo "<tr class = 'bordered'>";
						echo "<th class = 'bordered'>Date</th><th class = 'bordered'>Total Time</th><th class = 'bordered'>Distance</th><th class = 'bordered'>Comment</th><th class = 'bordered'>Area</th><th class = 'bordered'>Activity Name</th><th>Added</th>";
						echo "</tr>";
						while ($row = mysql_fetch_assoc($result)) {
						echo "<tr class = 'bordered'>";
						echo "<td class = 'bordered'>".$row['Date']."</td>";
						echo "<td class = 'bordered'>".$row['TotalTime']."</td>";
						echo "<td class = 'bordered'>".$row['Distance']."</td>";
						echo "<td class = 'bordered'>".$row['Comment']."</td>";
						echo "<td class = 'bordered'>".$row['Area']."</td>";
						echo "<td class = 'bordered'>".$row['ActivityName']."</td>";
						echo "<td class = 'bordered'>".$row['Added']."</td>";
						echo "</tr>";
						}
					
						echo "</table>";
					}
				?>
			</div>

			<div class="footer">
			</div>
		</div>
	</body>
</html>