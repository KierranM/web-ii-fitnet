<?php
	include "accessControl.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>FitNet - Change Profile Picture</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/fitnetstyle.css">
	<script src="javascript/jquery.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="javascript/editProfilePic.js"></script>
</head>
	<body>
		<div class="wrapper">
			<div class="header">
				<?php
					include 'connect.php';
					$select = 'SELECT FilePath FROM BannerImages ORDER BY RAND() LIMIT 1';
					$result = mysql_query($select);
					$row = mysql_fetch_assoc($result);
					mysql_free_result($result);
					$filePath = $row['FilePath'];
					echo "<img src='$filePath' alt='an image'>";
				?>
			</div>

			<div class="content">
				<?php
					include 'navbar.php';
					include 'profileNav.php';

					$error = false;

					if (isset($_POST['change'])) {
						$filePath = $_FILES['newProfilePic']['tmp_name'];
						$isImage = getimagesize($filePath);

						if ($isImage) {
							$myID = $_SESSION['userID'];
							$temp = preg_split('/\//', $_FILES['newProfilePic']['type']);
							$imageType = $temp[1];
							$filename = GetImageNameForUser($myID, $imageType);
							$path = realpath('.');
							$path = $path . "/Images/$filename";
							$cpResult = move_uploaded_file($filePath, $path);

							/*
								Commented out because this will no work on kate
								$update = "UPDATE Users SET ProfilePicture = '$path' WHERE UserID = $myID";
								$result = mysql_query($update);
							*/
						}
						else{
							$error = true;
						}
					}

					ShowForm($error);


					function ShowForm($error)
					{
						if ($error) {
							echo "<h3>Please Choose a valid image</h3>";						
						}
						echo "<h2>Choose Your Profile Picture</h2>";
							echo "<form action='editProfilePic.php' method='POST' enctype='multipart/form-data'>";
							echo "<input id='choice' type='file' name='newProfilePic' accept='image/*' required><b id='warning' class='hidden incorrect'>Please choose an image</b><br>";
							echo "<input type='submit' name='change' value='Change'>";
						echo "</form>";
					}

					function GetImageNameForUser($myID, $type)
					{
						$select = "SELECT FirstName, LastName FROM Users WHERE UserID = $myID";
						$result = mysql_query($select);

						$row = mysql_fetch_assoc($result);

						$imageName = $row['FirstName'] . "-" . $row['LastName'] . "." . $type;

						return $imageName;
					}
				?>
			</div>

			<div class="footer">
			</div>
		</div>
	</body>
</html>