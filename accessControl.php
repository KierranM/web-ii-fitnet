<?php
	session_start();

	include "connect.php";

	//Check if the displayName is set in POST
	if (isset($_POST['displayName'])) {
		//Retrieve the username from POST
		$userDisplayName = strip_tags($_POST['displayName']);
	}
	//Check if the displayName is set in SESSION
	elseif (isset($_SESSION['displayName'])) {
		//Retrieve the username from SESSION
		$userDisplayName = $_SESSION['displayName'];
	}

	//Check if the password is set in POST
	if (isset($_POST['password'])) {
		//Retrieve the password from POST and hash it
		$userPassword = strip_tags($_POST['password']);
		$userPassword = md5($userPassword);
	}
	//Check if password is set in SESSION
	elseif (isset($_SESSION['password'])) {
		//Retrieve the password from SESSION
		$userPassword = $_SESSION['password'];
	}

	//If the users displayName was not set, redirect to the login page
	if (!isset($userDisplayName)) {
		header("location: login.php");
	}
	else//The display name was set
	{
		$database = "mcphekt1_FitNet";
		$db = mysql_select_db($database);

		//verify username and password
		$select = "SELECT * FROM Users WHERE BINARY DisplayName = '$userDisplayName' AND BINARY password = '$userPassword'";
		$result = mysql_query($select);

		$user = mysql_fetch_assoc($result);

		//If there was a row in the query matching the given display name and password
		if (mysql_num_rows($result) > 0 ) {
			//Store the values in SESSION
			$_SESSION['displayName'] = $userDisplayName;
			$_SESSION['password'] = $userPassword;
			$_SESSION['userID'] = $user['UserID'];
			mysql_free_result($result);
		}
		else//There was no user matching those values
		{
			//Set failed attempt to true and redirect to the login page
			$_SESSION['failedAttempt'] = true;
			mysql_free_result($result);
			header("location: login.php");
		}
}