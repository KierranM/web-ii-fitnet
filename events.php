<?php 
	include 'accessControl.php';
	include 'connect.php';

	$myID = $_SESSION['userID'];
	$select = "SELECT a.ActivityName, w.Date, a.ColourCode FROM Workout AS w JOIN Activity AS a ON w.ActivityID = a.ActivityID WHERE w.UserID = $myID";

	$result = mysql_query($select);
	$events = array();
	while ($row = mysql_fetch_assoc($result))
	{
	    $eventArray = array();
		$eventArray['title'] = $row['ActivityName'];
	    $eventArray['start'] = $row['Date'];
	    $eventArray['color'] = $row['ColourCode'];
	    $events[] = $eventArray;
	}
	mysql_free_result($result);
	echo json_encode($events);