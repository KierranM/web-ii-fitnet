<?php
	include "accessControl.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>FitNet - Friends</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href='css/fitnetstyle.css'>
	<script src='javascript/jquery.js' type="text/javascript" charset="utf-8"></script>
	<script src='javascript/evenOutProfilePic.js' type="text/javascript" charset="utf-8"></script>
	<script src='javascript/alignFormInputs.js' type="text/javascript" charset="utf-8"></script>
</head>
	<body>
		<div class="wrapper">
			<div class="header">
				<?php
					include 'connect.php';
					$select = 'SELECT FilePath FROM BannerImages ORDER BY RAND() LIMIT 1';
					$result = mysql_query($select);
					$row = mysql_fetch_assoc($result);
					mysql_free_result($result);
					$filePath = $row['FilePath'];
					echo "<img src='$filePath' alt='an image'>";
				?>
			</div>

			<div class="content">
				<?php
					include 'navbar.php';

					$myID = $_SESSION['userID'];
					if (isset($_POST['Accept'])) {
						$friendID = strip_tags($_POST['friendID']);
						$update = "UPDATE Friends SET Accepted = TRUE WHERE UserID = $friendID AND FriendID = $myID";
						$insert = "INSERT INTO Friends (UserID, FriendID, Accepted) VALUES ($myID, $friendID, TRUE)";
						$result = mysql_query($update);
						$result = mysql_query($insert);
					}
					elseif (isset($_POST['Unfriend']) || isset($_POST['Decline'])) {
						$friendID = strip_tags($_POST['friendID']);

						$delete = "DELETE FROM Friends WHERE UserID = $myID AND FriendID = $friendID";
						$deleteTwo = "DELETE FROM Friends WHERE UserID = $friendID AND FriendID = $myID";

						$result = mysql_query($delete);
						$result = mysql_query($deleteTwo);
					}
					elseif (isset($_POST['Add'])) {
						$friendID = strip_tags($_POST['friendID']);
						$insert = "INSERT INTO Friends (FriendID, UserID) VALUES ($friendID,$myID)";
						$result = mysql_query($insert);
					}
					echo "<br>";
					echo "<br>";
					echo "<br>";
					echo "<br>";
					ShowFriendSearch();
					if (isset($_POST['search'])) {
						ShowSearchResults($myID);
					}
					echo "<br>";
					echo "<br>";
					ShowMyFriendRequests($myID);
					echo "<br>";
					echo "<br>";
					ShowMyFriends($myID);

					function ShowFriendSearch()
					{
						echo "<h2 class='noPadding noMargin clear'>Make New Friends</h2>";
						echo "<hr>";
						echo "<form action='friends.php' method='POST'>";
							echo "<label for='first'>First Name: </label>";
							echo "<input type='text' id='first' name='firstName'><br>";
							echo "<label for='last'>Last Name: </label>";
							echo "<input type='text' id='last' name='lastName'><br>";
							echo "<label for='displayName'>Display Name: </label>";
							echo "<input type='text' id='displayName' name='searchDisplayName'><br>";
							echo "<input type='submit' name='search' value='Search'>";
						echo "</form>";
					}

					function ShowSearchResults($myID)
					{
						$firstName = strip_tags($_POST['firstName']);
						$lastName = strip_tags($_POST['lastName']);
						$displayName = strip_tags($_POST['searchDisplayName']);
						$selectMyFriends = "SELECT DISTINCT u.* FROM Users AS u LEFT JOIN Friends as f ON u.UserID = f.FriendID WHERE u.UserID NOT IN(SELECT u.UserID FROM Users AS u JOIN Friends AS f ON u.UserID = f.FriendID WHERE f.UserID = $myID OR f.FriendID = $myID) AND u.UserID != $myID";

						$searchParts = array();
						if ($firstName != "") {
							$searchParts[] = "AND u.FirstName LIKE '%$firstName%'";
						}
						if ($lastName != "") {
							$searchParts[] = "AND u.LastName LIKE '%$lastName%'";
						}
						if ($displayName != "") {
							$searchParts[] = "AND u.DisplayName LIKE '%$displayName'";
						}
						$search = "" . implode(" ", $searchParts);
						if ($search != "") {
							$selectMyFriends .= " " . $search;
						}

						$result = mysql_query($selectMyFriends);

						if (mysql_num_rows($result) > 0) {
							while ($row = mysql_fetch_assoc($result)) {
								ShowFriend($row, $myID, "Add");
							}
						}
						else {
							echo "<h3>No one matched that search :(</h3>";
						}
					}


					function ShowMyFriends($myID)
					{
						echo "<h2 class='noPadding noMargin clear'>My Friends</h2>";
						echo "<hr>";
						$selectMyFriends = "SELECT * FROM Users AS u JOIN Friends AS f ON u.UserID = f.UserID WHERE f.FriendID = $myID AND f.Accepted = TRUE";
						$result = mysql_query($selectMyFriends);

						if (mysql_num_rows($result) > 0) {
							while ($row = mysql_fetch_assoc($result)) {
								ShowFriend($row, $myID, "Unfriend");
							}
						}
						else{
							echo "<b>You have no friends</b>";
						}
					}

					function ShowMyFriendRequests($myID)
					{
						echo "<h2 class='noPadding noMargin clear'>My Friend Requests</h2>";
						echo "<hr>";
						$selectMyFriends = "SELECT * FROM Users AS u JOIN Friends AS f ON u.UserID = f.UserID WHERE f.FriendID = $myID AND f.Accepted = FALSE";
						$result = mysql_query($selectMyFriends);
						
						if (mysql_num_rows($result) > 0) {
							while ($row = mysql_fetch_assoc($result)) {
								ShowFriend($row, $myID, "Accept","Decline");
							}
						}
						else{
							echo "<b>No friend requests</b>";
						}

					}

					function ShowFriend($values, $myID, $submitType, $submitTypeTwo = "NULL")
					{
						$friendID = $values['UserID'];
						$displayName = $values['DisplayName'];
						$firstName = $values['FirstName'];
						$lastName = $values['LastName'];
						$profilePic = $values['ProfilePicture'];
						$email = $values['Email'];
						$LastExercised = $values['LastExercised'];

						$d = strtotime($LastExercised);
						$betterDate = date("F j, Y",  $d);
						if ($betterDate == "January 1, 1970") {
							$betterDate = "Never";
						}
						else {
							$betterDate = "on " . $betterDate;
						}


						echo "<form action='friends.php' method='POST'>";
							echo "<div class='myFriend box'>";
								echo "<div class='profilePic'>";
									echo "<img src='$profilePic' height='32' width='32'>";
							echo "</div>";
							echo "<input type='submit' class='friendButton' name='$submitType' value='$submitType'>";
							if ($friendID != $myID) {
									echo "<a class='friendLink'  href='friendProfile.php?id=$friendID'><b>$displayName</b></a><br>";
							}
							else{
									echo "<b>$displayName</b><br>";
							}
							echo "<b class='smallText noPadding noMargin'>$firstName $lastName</b><br>";
							echo "<b class='smallText'>Last exercised $betterDate</b>";
							echo "<input type='hidden' name='friendID' value='$friendID'>";
							if ($submitTypeTwo != "NULL") {
								echo "<input type='submit' class='friendButton' name='$submitTypeTwo' value='$submitTypeTwo'>";
							}
							echo "</div>";
						echo "</form>";
					}
					
				?>
			</div>

			<div class="footer">
			</div>
		</div>
	</body>
</html>