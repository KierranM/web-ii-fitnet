<?php
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>FitNet - Login</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href='css/fitnetstyle.css'>
	<script src='javascript/jquery.js' type="text/javascript" charset="utf-8"></script>
	<script src='javascript/alignFormInputs.js' type="text/javascript" charset="utf-8"></script>
</head>
	<body>
	
		<div class="wrapper">
			<div class="header">
				<?php
					include 'connect.php';
					$select = 'SELECT FilePath FROM BannerImages ORDER BY RAND() LIMIT 1';
					$result = mysql_query($select);
					$row = mysql_fetch_assoc($result);
					mysql_free_result($result);
					$filePath = $row['FilePath'];
					echo "<img src='$filePath' alt='an image'>";
				?>
			</div>

			<div class="content">
				<?php
					echo "<form action='home.php' method='POST'>";
						echo "<label for='displayName'><b>Display Name: </b></label>";
						echo "<input id='displayName' type='text' name='displayName' placeholder='GeorgiePie'>";
						if (isset($_SESSION['failedAttempt']) && $_SESSION['failedAttempt'] === true) {
							echo "<b class='incorrect'>Unrecognised display name and/or password</b>";
							$_SESSION['failedAttempt'] = false;
						}
						echo "<br>";
						echo "<label for='password'><b>Password: </b></label>";
						echo "<input id='password' type='password' name='password'>";
						echo "<br>";
						echo "<br>";
						echo "<input class='dontAlign' type='submit' value='Login'>";
						echo "<a href='register.php'>Click here to register</a>";
					echo "</form>";
				?>
			</div>

			<div class="footer">
			</div>
		</div>
	</body>
</html>