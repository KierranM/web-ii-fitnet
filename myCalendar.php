<?php
	include "accessControl.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Fitnet - My Calender</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href='css/fitnetstyle.css'>
	<script src="javascript/jquery.js" type="text/javascript" charset="utf-8"></script>
	<script type='text/javascript' src='javascript/fullcalendar.js'></script>
	<script type='text/javascript' src='javascript/myCalendar.js'></script>
	<link rel='stylesheet' type='text/css' href='css/fullcalendar.css' />
	<link rel='stylesheet' type='text/css' href='css/fullcalendar.print.css' media='print' />
</head>
	<body>
		<div class="wrapper">
			<div class="header">
				<?php
					include 'connect.php';
					$select = 'SELECT FilePath FROM BannerImages ORDER BY RAND() LIMIT 1';
					$result = mysql_query($select);
					$row = mysql_fetch_assoc($result);
					mysql_free_result($result);
					$filePath = $row['FilePath'];
					echo "<img src='$filePath' alt='an image'>";
				?>
			</div>

			<div class="content">
				<?php include 'navbar.php';?>
				<br>
				<br>
				<div id='calendar'></div>
			</div>

			<div class="footer">
			</div>
		</div>
	</body>
</html>